package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class searchDetail_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_set_var_value_scope_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_if_test;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_set_var_value_scope_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_if_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
    _jspx_tagPool_c_set_var_value_scope_nobody.release();
    _jspx_tagPool_c_if_test.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      if (_jspx_meth_c_set_0(_jspx_page_context))
        return;
      out.write('\r');
      out.write('\n');
      if (_jspx_meth_c_set_1(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n");
      out.write("        <title>JSP Page</title>\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"./css/app.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://pro.fontawesome.com/releases/v5.15.1/css/all.css\" type=\"text/css\">\r\n");
      out.write("    </head>\r\n");
      out.write("    <style>\r\n");
      out.write("            @media all and (min-width: 992px) {\r\n");
      out.write("                .header__nav .nav-item .dropdown-menu{ display: none; }\r\n");
      out.write("                .header__nav .nav-item:hover .nav-link{   }\r\n");
      out.write("                .header__nav .nav-item:hover .dropdown-menu{ display: block; }\r\n");
      out.write("                .header__nav .nav-item .dropdown-menu{ margin-top:0; }\r\n");
      out.write("            }\r\n");
      out.write("        </style>\r\n");
      out.write("    <body>\r\n");
      out.write("        <section id=\"header\" style=\"background: #000000\">\r\n");
      out.write("            <div class=\"header-mobile\">\r\n");
      out.write("                <div class=\"mobile-toggle\"><i class=\"far fa-times close\"></i></div>\r\n");
      out.write("                <div class=\"header__logo\"><a class=\"header-brand\" href=\"./home\"><img src=\"./img/core-img/logo.png\" alt=\"\"></a></div>\r\n");
      out.write("                <div class=\"menu-nav\"> \r\n");
      out.write("                    <ul class=\"header__nav-moblie\">\r\n");
      out.write("                        <li><a class=\"current\" href=\"home\">Home </a></li>\r\n");
      out.write("                        <li><a href=\"./vpop\">Nhạc Việt Nam </a></li>\r\n");
      out.write("                        <li><a href=\"./us-uk\">Nhạc Quốc Tế </a></li>\r\n");
      out.write("                        <li><a href=\"./lofi\">Lofi </a></li>\r\n");
      out.write("                        <li>\r\n");
      out.write("                            ");

                                if (session.getAttribute("user") == null) {
                            
      out.write(" \r\n");
      out.write("                            <br>\r\n");
      out.write("                            <a href=\"login.jsp\">Đăng nhập</a>\r\n");
      out.write("                            ");

                                }
                            
      out.write("\r\n");
      out.write("                        </li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("\r\n");
      out.write("                     <div  class=\"header__search\" > \r\n");
      out.write("                            <form  value =\"txtSearch\" action=\"search\" method=\"post\" style=\"display: flex\">\r\n");
      out.write("                                <input type=\"text\" name=\"search\" placeholder=\"Search and hit enter...\" style=\"margin-top: 10px\">\r\n");
      out.write("                                <select name=\"searchType\" style=\"height: 30px; margin-top: 12px\">\r\n");
      out.write("                                    <option value=\"1\">Search By Name</option>\r\n");
      out.write("                                    <option value=\"2\">Search By Lyrics</option>\r\n");
      out.write("                                    <option value=\"3\">Search By Author</option>\r\n");
      out.write("\r\n");
      out.write("                                </select>\r\n");
      out.write("                                <button class=\"btn\" type=\"submit\"> <i class=\"fa fa-search\"> </i></button>\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("            ");
      out.write("                \r\n");
      out.write("            <div class=\"header__container container-fluid\">\r\n");
      out.write("                <div class=\"header__content\"> \r\n");
      out.write("                    <div class=\"header__logo\"><a class=\"header-brand\" href=\"./home\"><img src=\"./img/core-img/logo.png\" alt=\"\"></a></div>\r\n");
      out.write("                    <div class=\"header__menu\"> \r\n");
      out.write("                        <ul class=\"header__nav\">\r\n");
      out.write("                            ");
      if (_jspx_meth_c_if_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("                            ");
      if (_jspx_meth_c_if_1(_jspx_page_context))
        return;
      out.write("    \r\n");
      out.write("                            <li><a class=\"current\" href=\"./home\">Home </a></li>\r\n");
      out.write("                            <li class=\"nav-item dropdown\">\r\n");
      out.write("                                <a class=\"nav-link\" href=\"./vpop\">Nhạc Việt Nam </a>\r\n");
      out.write("                                <ul class=\"dropdown-menu\">\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${1}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Trẻ</a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${2}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Trữ Tình </a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${3}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Cách Mạng </a></li>\r\n");
      out.write("                                </ul>\r\n");
      out.write("                            </li>\r\n");
      out.write("                            <li class=\"nav-item dropdown\">\r\n");
      out.write("                                <a class=\"nav-link\" href=\"./us-uk\">Nhạc Quốc Tế </a>\r\n");
      out.write("                                <ul class=\"dropdown-menu\">\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${4}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc US-UK</a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${5}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc KPOP </a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${6}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Anime </a></li>\r\n");
      out.write("                                </ul>\r\n");
      out.write("                            </li>\r\n");
      out.write("                            <li class=\"nav-item dropdown\">\r\n");
      out.write("                                <a class=\"nav-link\" href=\"./lofi\">Lofi </a>\r\n");
      out.write("                                <ul class=\"dropdown-menu\">\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${8}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Piano</a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${9}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Guitar </a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${7}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Chill </a></li>\r\n");
      out.write("                                </ul>\r\n");
      out.write("                            </li>\r\n");
      out.write("                            \r\n");
      out.write("                            <li class=\"nav-item dropdown\">\r\n");
      out.write("                                <a class=\"nav-link\" href=\"./albums\">Albums </a>\r\n");
      out.write("                                <ul class=\"dropdown-menu\">\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"./playlist\">View PlayList </a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"./manageplaylist\">Manager PlayList </a></li>\r\n");
      out.write("                                </ul>\r\n");
      out.write("                            </li>\r\n");
      out.write("                            \r\n");
      out.write("                        </ul>\r\n");
      out.write("                        <div class=\"header__search\"> \r\n");
      out.write("                            <form value =\"txtSearch\" action=\"search\" method=\"post\" style=\"display: flex\">\r\n");
      out.write("                                <input type=\"text\" name=\"txtsearch\" placeholder=\"Search and hit enter...\" style=\"margin-top: 10px\">\r\n");
      out.write("                                <select name=\"searchType\" style=\"height: 30px; margin-top: 12px\">\r\n");
      out.write("                                    <option value=\"1\">Search By Name</option>\r\n");
      out.write("                                    <option value=\"2\">Search By Lyrics</option>\r\n");
      out.write("                                    <option value=\"3\">Search By Author</option>\r\n");
      out.write("\r\n");
      out.write("                                </select>\r\n");
      out.write("                                <button class=\"btn\" type=\"submit\"> <i class=\"fa fa-search\"> </i></button>\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <ul class=\"header__social\"> \r\n");
      out.write("                            <li><a class=\"fab fa-facebook\" href=\"#\"> </a></li>\r\n");
      out.write("                            <li><a class=\"fab fa-twitter\" href=\"#\"> </a></li>\r\n");
      out.write("                            <li><a class=\"fab fa-youtube\" href=\"#\"></a></li>\r\n");
      out.write("                            <li class=\"login\">\r\n");
      out.write("                                ");

                                    if (session.getAttribute("user") == null) {
                                
      out.write("                           \r\n");
      out.write("                                <a href=\"login.jsp\">Đăng nhập</a>\r\n");
      out.write("                                ");

                                    }
                                
      out.write("\r\n");
      out.write("                            </li>\r\n");
      out.write("\r\n");
      out.write("                            ");
      if (_jspx_meth_c_if_2(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\r\n");
      out.write("                        </ul>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"mobile-toggle\"><i class=\"far fa-bars open\"></i><i class=\"far fa-times close\">  </i></div>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"header__background\"></div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </section>\r\n");
      out.write("        <section id=\"recommend\">\r\n");
      out.write("            <div class=\"container\">\r\n");
      out.write("                <div class=\"group-tab__content\">\r\n");
      out.write("                    <div class=\"tab__container\">\r\n");
      out.write("                        <div class=\"tab__heading\">\r\n");
      out.write("                            <h2>Bài Hát</h2>\r\n");
      out.write("                            <div class=\"btn-group\">\r\n");
      out.write("                                <div class=\"btn btn--second btn-addList option\"> <i class=\"fas fa-download\"> <span>Tải về</span></i></div>\r\n");
      out.write("                                <div class=\"btn btn-playAll\"> <i class=\"fas fa-play\"><span>Phát tất cả</span></i></div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <div class=\"tab__content vpop-tab active\">\r\n");
      out.write("                            <div class=\"container__slide hide-on-mobile\">\r\n");
      out.write("                                <div class=\"container__slide-show\">\r\n");
      out.write("                                    ");
 int index = 0;
      out.write("\r\n");
      out.write("                                    ");
      //  c:forEach
      org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
      _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
      _jspx_th_c_forEach_0.setParent(null);
      _jspx_th_c_forEach_0.setVar("list");
      _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.listAuthor}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
      int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
      try {
        int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
        if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
          do {
            out.write("\r\n");
            out.write("                                        ");
 index++;
            out.write("\r\n");
            out.write("                                        <div class=\"container__slide-item ");
 switch (index) {
                                                case 1: 
            out.write("first");
 break;
                                                    case 2: 
            out.write("second");
 break;
                                                        case 3: 
            out.write("third");
 break;
                                                            default: 
            out.write("fourth");
 break;
                                                            }
            out.write("\">\r\n");
            out.write("                                            <div class=\"container__slide-img\" style=\"background: url('");
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${list.image}", java.lang.String.class, (PageContext)_jspx_page_context, null));
            out.write("') no-repeat center center / cover\">\r\n");
            out.write("                                            </div>\r\n");
            out.write("                                        </div>\r\n");
            out.write("                                    ");
            int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
            if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
              break;
          } while (true);
        }
        if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
          return;
        }
      } catch (Throwable _jspx_exception) {
        while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
          out = _jspx_page_context.popBody();
        _jspx_th_c_forEach_0.doCatch(_jspx_exception);
      } finally {
        _jspx_th_c_forEach_0.doFinally();
        _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
      }
      out.write("\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"tab__container-list\">\r\n");
      out.write("                                <div class=\"play-list scroll-overflow\"> \r\n");
      out.write("                                    ");
      if (_jspx_meth_c_forEach_1(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div> \r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("        </section>\r\n");
      out.write("        <section id=\"footer\">\r\n");
      out.write("            <div class=\"container footer__container\">\r\n");
      out.write("                <div class=\"footer__about\"> \r\n");
      out.write("                    <h2>About Us</h2>\r\n");
      out.write("                    <p>It is a long established fact that a reader will be distracted by the readable content.</p>\r\n");
      out.write("                    <p>&copy; Copyright &copy; 2022 <span>Group Four</span></p>\r\n");
      out.write("                </div>\r\n");
      out.write("                <ul class=\"footer__categories\"> \r\n");
      out.write("                    <h2>Categories</h2>\r\n");
      out.write("                    <li><a href=\"#\">Entrepreneurship </a></li>\r\n");
      out.write("                    <li><a href=\"#\">Media </a></li>\r\n");
      out.write("                    <li><a href=\"#\">Tech </a></li>\r\n");
      out.write("                    <li>   <a href=\"#\">Tutorials </a></li>\r\n");
      out.write("                </ul>\r\n");
      out.write("                <div class=\"footer_social\"> \r\n");
      out.write("                    <h2>Follow Us</h2>\r\n");
      out.write("                    <ul class=\"media\">\r\n");
      out.write("                        <li><a class=\"fab fa-facebook\" href=\"#\"> </a></li>\r\n");
      out.write("                        <li><a class=\"fab fa-twitter\" href=\"#\"> </a></li>\r\n");
      out.write("                        <li><a class=\"fab fa-pinterest\" href=\"#\"> </a></li>\r\n");
      out.write("                        <li><a class=\"fab fa-instagram\" href=\"#\"> </a></li>\r\n");
      out.write("                        <li><a class=\"fab fa-youtube\" href=\"#\"> </a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                    <ul class=\"store\"> \r\n");
      out.write("                        <li> <a href=\"\"><img src=\"./img/core-img/app-store.png\" alt=\"\"></a></li>\r\n");
      out.write("                        <li> <a href=\"\"><img src=\"./img/core-img/google-play.png\" alt=\"\"></a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </section>\r\n");
      out.write("        <section id=\"toast\"></section>\r\n");
      out.write("        <audio id=\"audio\" src=\"\">    </audio>\r\n");
      out.write("        <script src=\"./node_modules/jarallax/dist/jarallax.min.js\"></script>\r\n");
      out.write("        <script src=\"./js/app.js\"></script>\r\n");
      out.write("        <script src=\"./js/jquery-3.2.1.min.js\"></script>\r\n");
      out.write("        <script src=\"./js/handle_ajax.js\"></script>\r\n");
      out.write("        <script src=\"./js/handle_toast.js\"></script>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_set_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_scope_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_0.setPageContext(_jspx_page_context);
    _jspx_th_c_set_0.setParent(null);
    _jspx_th_c_set_0.setVar("user");
    _jspx_th_c_set_0.setScope("page");
    _jspx_th_c_set_0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.user}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_set_0 = _jspx_th_c_set_0.doStartTag();
    if (_jspx_th_c_set_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_0);
      return true;
    }
    _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_0);
    return false;
  }

  private boolean _jspx_meth_c_set_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_1 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_scope_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_1.setPageContext(_jspx_page_context);
    _jspx_th_c_set_1.setParent(null);
    _jspx_th_c_set_1.setVar("list");
    _jspx_th_c_set_1.setScope("page");
    _jspx_th_c_set_1.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.listP}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_set_1 = _jspx_th_c_set_1.doStartTag();
    if (_jspx_th_c_set_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_1);
      return true;
    }
    _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_1);
    return false;
  }

  private boolean _jspx_meth_c_if_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_0.setPageContext(_jspx_page_context);
    _jspx_th_c_if_0.setParent(null);
    _jspx_th_c_if_0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.role_ID == 1}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_0 = _jspx_th_c_if_0.doStartTag();
    if (_jspx_eval_c_if_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                                <li class=\"nav-item dropdown\">\r\n");
        out.write("                                    <a class=\"nav-link\" href=\"./listacc\">Manage Account </a>\r\n");
        out.write("                                    <ul class=\"dropdown-menu\">\r\n");
        out.write("                                        <li><a class=\"dropdown-item\" href=\"./manageAlbum\">Manage Albums</a></li>\r\n");
        out.write("                                        <li><a class=\"dropdown-item\" href=\"./manager\">Manage Songs</a></li>\r\n");
        out.write("                                        <li><a class=\"dropdown-item\" href=\"./managecomment\">Manage Comment</a></li>\r\n");
        out.write("                                    </ul>\r\n");
        out.write("                                </li>\r\n");
        out.write("                            ");
        int evalDoAfterBody = _jspx_th_c_if_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
    return false;
  }

  private boolean _jspx_meth_c_if_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_1 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_1.setPageContext(_jspx_page_context);
    _jspx_th_c_if_1.setParent(null);
    _jspx_th_c_if_1.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.role_ID == 3}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_1 = _jspx_th_c_if_1.doStartTag();
    if (_jspx_eval_c_if_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                                <li><a href=\"./manager\">Manage Songs</a></li>\r\n");
        out.write("                                ");
        int evalDoAfterBody = _jspx_th_c_if_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
    return false;
  }

  private boolean _jspx_meth_c_if_2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_2 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_2.setPageContext(_jspx_page_context);
    _jspx_th_c_if_2.setParent(null);
    _jspx_th_c_if_2.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user != null}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_2 = _jspx_th_c_if_2.doStartTag();
    if (_jspx_eval_c_if_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                                <li>\r\n");
        out.write("                                    <div class=\"user\"> \r\n");
        out.write("                                        <a href=\"UpdateProfile\"><div class=\"user__avatar bg-img\" style=\"background-image: url('./img/core-img/avatar-default-1.jpg');\"></div></a>\r\n");
        out.write("                                        <a class=\"fas fa-sign-out-alt icon\" href=\"signout\"></a>\r\n");
        out.write("                                        <div class=\"user__option\"> \r\n");
        out.write("                                            <div class=\"user__option-content\"> \r\n");
        out.write("                                                <div class=\"option-item view-info\">\r\n");
        out.write("                                                    <div class=\"user__avatar bg-img\" style=\"background-image: url('./img/core-img/avatar-default-1.jpg');\"></div>\r\n");
        out.write("                                                    <div class=\"user__info\"> \r\n");
        out.write("                                                        <div class=\"user__name heading\">");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.username}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write(" </div>\r\n");
        out.write("                                                        <div class=\"subtitle\">See your profile</div>\r\n");
        out.write("                                                    </div>\r\n");
        out.write("                                                </div>\r\n");
        out.write("                                                <div class=\"option-item setting\"><i class=\"fas fa-cog icon\"></i>\r\n");
        out.write("                                                    <div class=\"heading\">Settings </div>\r\n");
        out.write("                                                </div>\r\n");
        out.write("                                                <form action=\"sign\">\r\n");
        out.write("                                                    <input type=\"text\" name=\"url\" value=\"home\" hidden>\r\n");
        out.write("                                                    <label for=\"user__sign-out--pc\">\r\n");
        out.write("                                                        <div class=\"option-item logout\"><i class=\"fas fa-sign-out-alt icon\"></i>\r\n");
        out.write("                                                            <div class=\"heading\">Log Out</div>\r\n");
        out.write("                                                        </div>\r\n");
        out.write("                                                    </label>\r\n");
        out.write("                                                    <input type=\"submit\" hidden id=\"user__sign-out--pc\">\r\n");
        out.write("                                                </form>\r\n");
        out.write("                                            </div>\r\n");
        out.write("                                        </div>\r\n");
        out.write("                                    </div>\r\n");
        out.write("                                </li>\r\n");
        out.write("                            ");
        int evalDoAfterBody = _jspx_th_c_if_2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_2);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_2);
    return false;
  }

  private boolean _jspx_meth_c_forEach_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_1 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_1.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_1.setParent(null);
    _jspx_th_c_forEach_1.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.listAuthor}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_1.setVar("list");
    int[] _jspx_push_body_count_c_forEach_1 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_1 = _jspx_th_c_forEach_1.doStartTag();
      if (_jspx_eval_c_forEach_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("                                        <div class=\"song-item\" data-path=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${list.path}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">\r\n");
          out.write("                                            <div class=\"song-content\">\r\n");
          out.write("                                                <div class=\"song-img bg-img\" style=\"background-image: url('");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${list.image}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("');\">\r\n");
          out.write("                                                    <div class=\"song-img--hover\"><i class=\"fas fa-play\"></i></div>\r\n");
          out.write("                                                </div>\r\n");
          out.write("                                                <div class=\"song-info\"> \r\n");
          out.write("                                                    <h2 class=\"song-name\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${list.name}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</h2>\r\n");
          out.write("                                                    <div class=\"song-author\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${list.author}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</div>\r\n");
          out.write("                                                </div>\r\n");
          out.write("                                            </div>\r\n");
          out.write("                                            <div class=\"song-duration\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${list.duration}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</div>\r\n");
          out.write("                                            <div class=\"player__content\">\r\n");
          out.write("                                                <div class=\"player__control\"> \r\n");
          out.write("                                                    <div class=\"play\"><i class=\"fas fa-play-circle\"></i></div>\r\n");
          out.write("                                                    <div class=\"pause active\"><i class=\"fas fa-pause-circle\"></i></div><span class=\"currenTime\">00:00</span>\r\n");
          out.write("                                                    <div class=\"sidebarTime--bg\">\r\n");
          out.write("                                                        <div class=\"sidebarTime--current\"></div>\r\n");
          out.write("                                                    </div><span class=\"duration\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${list.duration}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</span>\r\n");
          out.write("                                                </div>\r\n");
          out.write("                                            </div>\r\n");
          out.write("                                        </div>\r\n");
          out.write("                                    ");
          int evalDoAfterBody = _jspx_th_c_forEach_1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_1[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_1.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_1.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_1);
    }
    return false;
  }
}
