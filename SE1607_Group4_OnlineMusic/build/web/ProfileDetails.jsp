<%-- 
    Document   : adminProfileDetails
    Created on : May 21, 2022, 7:00:37 PM
    Author     : Black
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="p" scope="page" value="${requestScope.profiledetails}" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Profile Detail</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    </head>
    <body style="background-color: gray">
        <section class="section about-section" id="about">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-lg-6" >
                        <div class="about-text go-to">
                            <h3 class="dark-color" style="color: white"><b>Profile Information</b></h3>
                            <div class="row about-list">
                                <div class="col-md-6"  >
                                    <div class="media">
                                        <label style="color: black; font-size: 20px"><b>Username: </b></label>
                                        <p style="color: black; font-size: 20px; margin-left: 20px">${p.username}</p>
                                    </div>
                                    <div class="media">
                                        <label style="color: black; font-size: 20px"><b>Password:</b></label>
                                        <p style="color: black; font-size: 20px; margin-left: 20px">${p.password}</p>
                                    </div>
                                    <div class="media">
                                        <label style="color: black; font-size: 20px"><b>User Email:</b></label>
                                        <p style="color: black; font-size: 20px; margin-left: 20px">${p.email}</p>
                                    </div>
                                    <div class="media">
                                        <label style="color: black; font-size: 20px"><b>First Name:</b></label>
                                        <c:if test="${(p.first_name == null) or (p.first_name.isEmpty())}"><p style="color: black; font-size: 20px; margin-left: 20px">Unknown</p></c:if>
                                        <c:if test="${p.first_name != null}"><p style="color: black; font-size: 20px; margin-left: 20px">${p.first_name}</p></c:if>
                                        </div>
                                        <div class="media">
                                            <label style="color: black; font-size: 20px"><b>Last Name:</b></label>
                                        <c:if test="${(p.last_name == null) or (p.last_name.isEmpty())}"><p style="color: black; font-size: 20px; margin-left: 20px">Unknown</p></c:if>
                                        <c:if test="${p.first_name != null}"><p style="color: black; font-size: 20px; margin-left: 20px">${p.last_name}</p></c:if>
                                        </div>
                                        <div class="media">
                                            <label style="color: black; font-size: 20px"><b>Address:</b></label>
                                        <c:if test="${p.address == null}"><p style="color: black; font-size: 20px; margin-left: 20px">Unknown</p></c:if>
                                        <c:if test="${p.address != null}"><p style="color: black; font-size: 20px; margin-left: 20px">${p.address}</p></c:if>
                                        </div>
                                        <div class="media">
                                            <label style="color: black; font-size: 20px"><b>Birth Date:</b></label>
                                        <c:if test="${p.dob == null}"><p style="color: black; font-size: 20px; margin-left: 20px">Unknown</p></c:if>
                                        <c:if test="${p.dob != null}"><p style="color: black; font-size: 20px; margin-left: 20px">${p.dob}</p></c:if>
                                        </div>
                                        <div class="media">
                                            <label style="color: black; font-size: 20px"><b>Gender:</b></label>
                                        <c:if test="${p.gender == null}"><p style="color: black; font-size: 20px; margin-left: 20px">Unknown</p></c:if>
                                        <c:if test="${p.gender == 'Female'}"><p style="color: black; font-size: 20px; margin-left: 20px">Female</p></c:if>
                                        <c:if test="${p.gender == 'Male'}"><p style="color: black; font-size: 20px; margin-left: 20px">Male</p></c:if>
                                        </div>
                                        <div class="media">
                                            <label style="color: black; font-size: 20px"><b>Join Time:</b></label>
                                            <p style="color: black; font-size: 20px; margin-left: 20px">${p.t_create}</p>
                                    </div>
                                    <div class="media">
                                            <label style="color: black; font-size: 20px"><b>Last Online:</b></label>
                                            <c:if test="${p.t_lastOnline == null}"><p style="color: black; font-size: 20px; margin-left: 20px">Unknown</p></c:if>
                                        <c:if test="${p.t_lastOnline != null}"><p style="color: black; font-size: 20px; margin-left: 20px">${p.t_lastOnline}</p></c:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-avatar">
                            <c:if test="${(p.avatar == null) or (p.avatar.isEmpty())}">
                            <img style="border-radius: 50%; margin-left: 100px" src="./img/avatar.jpg" width="400" height="400">
                            </c:if>
                            <c:if test="${p.avatar != null}">
                            <img style="border-radius: 50%; margin-left: 100px" src="./img/uploads/${p.avatar}" width="300" height="300">
                            </c:if>
                        </div>
                    </div>
                </div>
                <form style="margin-top: 10px">
                    <input type="button" class="btn btn-danger" value="Back" onclick="history.back()">
                </form>
            </div>
        </section>

        <style type="text/css">
            body{
                color: #6F8BA4;
                margin-top:20px;
            }
            .section {
                padding: 100px 0;
                position: relative;
            }
            .gray-bg {
                background-color: #f5f5f5;
            }
            img {
                max-width: 100%;
            }
            img {
                vertical-align: middle;
                border-style: none;
            }
            /* About Me 
            ---------------------*/
            .about-text h3 {
                font-size: 45px;
                font-weight: 700;
                margin: 0 0 6px;
            }
            @media (max-width: 767px) {
                .about-text h3 {
                    font-size: 35px;
                }
            }
            .about-text h6 {
                font-weight: 600;
                margin-bottom: 15px;
            }
            @media (max-width: 767px) {
                .about-text h6 {
                    font-size: 18px;
                }
            }
            .about-text p {
                font-size: 18px;
                max-width: 450px;
            }
            .about-text p mark {
                font-weight: 600;
                color: #20247b;
            }

            .about-list {
                padding-top: 10px;
            }
            .about-list .media {
                padding: 5px 0;
            }
            .about-list label {
                color: #20247b;
                font-weight: 600;
                width: 100px;
                margin: 0;
                position: relative;
            }

            .about-list p {
                margin: 0;
                font-size: 15px;
            }

            @media (max-width: 991px) {
                .about-avatar {
                    margin-top: 30px;
                }
            }

            .about-section .counter {
                padding: 22px 20px;
                background: #ffffff;
                border-radius: 10px;
                box-shadow: 0 0 30px rgba(31, 45, 61, 0.125);
            }
            .about-section .counter .count-data {
                margin-top: 10px;
                margin-bottom: 10px;
            }
            .about-section .counter .count {
                font-weight: 700;
                color: #20247b;
                margin: 0 0 5px;
            }
            .about-section .counter p {
                font-weight: 600;
                margin: 0;
            }
            mark {
                background-image: linear-gradient(rgba(252, 83, 86, 0.6), rgba(252, 83, 86, 0.6));
                background-size: 100% 3px;
                background-repeat: no-repeat;
                background-position: 0 bottom;
                background-color: transparent;
                padding: 0;
                color: currentColor;
            }
            .theme-color {
                color: #fc5356;
            }
            .dark-color {
                color: #20247b;
            }
        </style>
    </body>
</html>