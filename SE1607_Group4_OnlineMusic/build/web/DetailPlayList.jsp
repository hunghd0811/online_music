
<%@page import="java.util.List"%>
<%@page import="model.Songs"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : home
    Created on : 28-05-2022
    Author     : duy
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="user" scope="page" value="${sessionScope.user}" />
<c:set var="album1" scope="page" value="${requestScope.album1}" />
<c:set var="album2" scope="page" value="${requestScope.album2}" />
<c:set var="album3" scope="page" value="${requestScope.album3}" />
<c:set var="id" scope="page" value="${requestScope.id}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>PlayList Song</title>
        <link href="css/manageSongs.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="./css/app.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.1/css/all.css" type="text/css">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    </head>
    <style>
        img.card-img-top{
            width: 90%;
            height: 300px;
        }
        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-left: 60px;
        }
        .col-lg-4 {
            flex: 0 0 33.333333%;
        }
        .btn{
            margin-left: 200px;
            margin-bottom: 30px;
        }
    </style>
    <body>
        <jsp:include page="Header.jsp"/>  

        <%--post songs of album--%>
        <section id="recommend">
            <div class="container">
                <div class="group-tab__content">
                    <div class="tab__container">
                        <div class="tab__heading">
                            <h2>Bài Hát</h2>

                        </div>
                        <div class="tab__content vpop-tab active">
                            <div class="tab__container-list">
                                <div class="play-list scroll-overflow">
                                    <c:forEach var="o" items="${requestScope.detailPlaylist}">
                                        <div class="song-item" data-path="${o.path}">
                                            <div class="song-content">
                                                <div class="song-img bg-img" style="background-image: url('${o.image}');">
                                                    <div class="song-img--hover"><i class="fas fa-play"></i></div>
                                                </div>
                                                <div class="song-info"> 
                                                    <h2 class="song-name">${o.name}</h2>
                                                    <div class="song-author">${o.author}</div>
                                                </div>
                                            </div>
                                            <div class="song-duration">${o.duration}</div>
                                            <div class="player__content">
                                                <div class="player__control"> 
                                                    <div class="play"><i class="fas fa-play-circle"></i></div>
                                                    <div class="pause active"><i class="fas fa-pause-circle"></i></div><span class="currenTime">00:00</span>
                                                    <div class="sidebarTime--bg">
                                                        <div class="sidebarTime--current"></div>
                                                    </div><span class="duration">${o.duration}</span>
                                                </div>
                                            </div>

                                        </div>
                                    </c:forEach>
                                </div>
                            </div>      
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <%--newletter--%>
        <section id="newletter">
            <div class="jarallax bg-overlay bg-img" style="background-image: url('./img/core-img/jarallax.jpg');">
                <div class="container newletter__container">
                    <div class="newletter__content">
                        <h1>Sign Up To Newsletter</h1>
                        <p>Subscribe to receive info on our latest news and episodes</p>
                    </div>
                    <div class="newletter__subcribe">
                        <form action="" method="method">
                            <input type="text" placeholder="Your Email">
                            <input type="submit" value="SUBCRIBE">
                        </form>
                    </div>
                </div>
            </div>
        </div
    </section>
    <section id="footer">
        <div class="container footer__container">
            <div class="footer__about"> 
                <h2>About Us</h2>
                <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                <p>&copy; Copyright &copy; 2022 <span>Group Four</span></p>
            </div>
            <ul class="footer__categories"> 
                <h2>Categories</h2>
                <li><a href="#">Entrepreneurship </a></li>
                <li><a href="#">Media </a></li>
                <li><a href="#">Tech </a></li>
                <li>   <a href="#">Tutorials </a></li>
            </ul>
            <div class="footer_social"> 
                <h2>Follow Us</h2>
                <ul class="media">
                    <li><a class="fab fa-facebook" href="#"> </a></li>
                    <li><a class="fab fa-twitter" href="#"> </a></li>
                    <li><a class="fab fa-pinterest" href="#"> </a></li>
                    <li><a class="fab fa-instagram" href="#"> </a></li>
                    <li><a class="fab fa-youtube" href="#"> </a></li>
                </ul>
                <ul class="store"> 
                    <li> <a href=""><img src="./img/core-img/app-store.png" alt=""></a></li>
                    <li> <a href=""><img src="./img/core-img/google-play.png" alt=""></a></li>
                </ul>
            </div>
        </div>
    </section>
    
    <section id="toast"></section>
    <audio id="audio" src="">    </audio>
    <script src="./node_modules/jarallax/dist/jarallax.min.js"></script>
    <script src="./js/app.js"></script>
    <script src="./js/jquery-3.2.1.min.js"></script>
    <script src="./js/handle_ajax.js"></script>
    <script src="./js/handle_toast.js"></script>

</body>
</html>


