
<%@page import="java.util.List"%>
<%@page import="model.Songs"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : home
    Created on : 28-05-2022
    Author     : duy
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="user" scope="page" value="${sessionScope.user}" />
<c:set var="album" scope="page" value="${requestScope.album}" />
<c:set var="id" scope="page" value="${requestScope.id}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Albums</title>
        <link rel="stylesheet" href="./css/app.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.1/css/all.css" type="text/css">
            <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    </head>
    <style>
        .slider__item .slider__content {
            margin-bottom: 500px;
        }
        @media all and (min-width: 992px) {
            .header__nav .nav-item .dropdown-menu{ display: none; }
            .header__nav .nav-item:hover .nav-link{   }
            .header__nav .nav-item:hover .dropdown-menu{ display: block; background-color: #e2264d }
            .header__nav .nav-item .dropdown-menu{ margin-top:0; }
        }
        .row {
            display: block;
            margin-left: 60px;
        }
        .col-sm-4 {
            flex: 0 0 33.333333%;
        }
        img.card-img-top{
            width: 300px; 
            height: 300px;
            margin-right: 60px;
        }

        .album-detail{
            display: inline-grid;
        }
        .album-detail .btn{
            margin-left: 90px;
            margin-right: 150px;
            margin-bottom: 100px;
        }
        album-btn{
            width: 400px;
            height: 400px;
        }
        select{
            background-color: #616161;
            color: white;
            border-radius: 10px;
        }
        .btn {
            position: relative;
            z-index: 1;
            height: 46px;
            line-height: 43px;
            font-size: 14px;
            font-weight: 600;
            display: inline-block;
            padding: 0 20px;
            text-align: center;
            text-transform: uppercase;
            border-radius: 30px;
            -webkit-transition: all 500ms;
            -o-transition: all 500ms;
            transition: all 500ms;
            border: 2px solid #f55656;
            letter-spacing: 1px;
            box-shadow: none;
            background-color: #f55656;
            color: #ffffff;
            cursor: pointer;
        }
        a, a:active, a:focus, a:hover {
            color: #232323;
            text-decoration: none;
            -webkit-transition-duration: 500ms;
            -o-transition-duration: 500ms;
            transition-duration: 500ms;
            outline: none;
        }
    </style>
    <body>       
        <%--Hearder--%>
        <section id="header" style="background: #000">
            <div class="header-mobile">
                <div class="mobile-toggle"><i class="far fa-times close"></i></div>
                <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                <div class="menu-nav"> 
                    <ul class="header__nav-moblie">
                        <li>
                            <%
                                if (session.getAttribute("user") == null) {
                            %> 
                            <br>
                            <a href="login.jsp">Đăng nhập</a>
                            <%
                                }
                            %>
                        </li>
                    </ul>

                    <%--Search--%>
                    <div class="header__search"> 
                        <form value="txtSearch" action="search" method="post" style="display : flex;">
                            <input type="text" name="search" placeholder="Search and hit enter..." style="margin-top: 10px">
                            <select name="searchType" style="height: 30px; margin-top: 12px;">
                                <option value="1">Search By Name</option>
                                <option value="2">Search By Lyrics</option>
                                <option value="3">Search By Author</option>

                            </select>
                            <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                        </form>
                    </div>
                </div>
            </div>
            <%--Hearder container--%>                
            <div class="header__container container-fluid">
                <div class="header__content" style="background-color: #000"> 
                    <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                    <div class="header__menu"> 
                        <ul class="header__nav">
                            <c:if test="${user.role_ID == 1}">
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="./listacc">Manage Account </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="./manageAlbum">Manage Albums</a></li>
                                        <li><a class="dropdown-item" href="./manager">Manage Songs</a></li>
                                        <li><a class="dropdown-item" href="./managecomment">Manage Comment</a></li>
                                        <li><a class="dropdown-item" href="managehistory">Manage View History</a></li>
                                    </ul>
                                </li>
                            </c:if>
                            <c:if test="${user.role_ID == 3}">
                                <li><a href="./manager">Manage Songs</a></li>
                                </c:if>    
                            <li><a  href="./home">Home </a></li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${1}">Nhạc Việt Nam </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${1}"> Nhạc Trẻ</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${2}"> Nhạc Trữ Tình </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${3}"> Nhạc Cách Mạng </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${2}">Nhạc Quốc Tế </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${4}"> Nhạc US-UK</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${5}"> Nhạc KPOP </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${6}"> Nhạc Anime </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${3}">Lofi </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${8}"> Nhạc Piano</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${9}"> Nhạc Guitar </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${7}"> Nhạc Chill </a></li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link current" href="./albums">Albums </a>

                                <c:if test="${user != null}">
                                <li><a href="./allplaylist">View PlayList </a></li>                                
                                </c:if>
                            </li>
                            <c:if test="${user != null}">
                                <li><a href="history">Recently Visited</a></li>
                                </c:if>

                        </ul>
                        <div class="header__search"> 
                            <form value ="txtSearch" action="search" method="post" style="display: flex">
                                <input type="text" name="txtsearch" placeholder="Search and hit enter..." style="margin-top: 10px">
                                <select name="searchType" style="height: 30px; margin-top: 12px">
                                    <option value="1">Search By Name</option>
                                    <option value="2">Search By Lyrics</option>
                                    <option value="3">Search By Author</option>

                                </select>
                                <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                            </form>
                        </div>
                        <ul class="header__social">      
                            <li class="login">
                                <%
                                    if (session.getAttribute("user") == null) {
                                %>                           
                                <a href="login.jsp">Đăng nhập</a>
                                <%
                                    }
                                %>
                            </li>

                            <c:if test="${user != null}">
                                <li>
                                    <div class="user"> 
                                        <a href="UpdateProfile"><div class="user__avatar bg-img" 
                                                                     <c:if test="${(user.avatar == null) or (user.avatar.isEmpty())}">
                                                                         style="background-image: url('./img/avatar.jpg');"
                                                                     </c:if>
                                                                     <c:if test="${user.avatar != null}">
                                                                         style="background-image: url('./img/uploads/${user.avatar}');"
                                                                     </c:if>
                                                                     ></div></a>
                                        <a class="fas fa-sign-out-alt icon" href="signout"></a>
                                        <div class="user__option"> 
                                            <div class="user__option-content"> 
                                                <div class="option-item view-info">
                                                    <div class="user__avatar bg-img" style="background-image: url('./img/core-img/avatar-default-1.jpg');"></div>
                                                    <div class="user__info"> 
                                                        <div class="user__name heading">${user.username} </div>
                                                        <div class="subtitle">See your profile</div>
                                                    </div>
                                                </div>
                                                <div class="option-item setting"><i class="fas fa-cog icon"></i>
                                                    <div class="heading">Settings </div>
                                                </div>
                                                <form action="sign">
                                                    <input type="text" name="url" value="home" hidden>
                                                    <label for="user__sign-out--pc">
                                                        <div class="option-item logout"><i class="fas fa-sign-out-alt icon"></i>
                                                            <div class="heading">Log Out</div>
                                                        </div>
                                                    </label>
                                                    <input type="submit" hidden id="user__sign-out--pc">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </c:if>

                        </ul>
                    </div>
                    <div class="mobile-toggle"><i class="far fa-bars open"></i><i class="far fa-times close">  </i></div>
                </div>
                <div class="header__background"></div>
            </div>
        </section>  
        <%--Albums--%>

        <section id="recommend">
            <div class="row">
                <c:forEach var="a" items="${album}">
                    <div class="album-detail ">
                        <img class="card-img-top" style="background: url('${a.image}') no-repeat center center / cover">
                        <h5><b>${a.name}</b> </h5>
                        <a class="btn w-100 rounded my-2" href="tab?id=${a.albums_ID}">Play Album</a>
                    </div>
                </c:forEach>  
            </div>                   
        </section>

        <%--newletter--%>
        <section id="newletter">
            <div class="jarallax bg-overlay bg-img" style="background-image: url('./img/core-img/jarallax.jpg');">
                <div class="container newletter__container">
                    <div class="newletter__content">
                        <h1>Sign Up To Newsletter</h1>
                        <p>Subscribe to receive info on our latest news and episodes</p>
                    </div>
                    <div class="newletter__subcribe">
                        <form action="" method="method">
                            <input type="text" placeholder="Your Email">
                            <input type="submit" value="SUBCRIBE">
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section id="footer">
            <div class="container footer__container" style="display: flex">
                <div class="footer__about"> 
                    <h2>About Us</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                    <p>&copy; Copyright &copy; 2022 <span>Group Four</span></p>
                </div>
                <ul class="footer__categories"> 
                    <h2>Categories</h2>
                    <li><a href="#" style="color: black">Entrepreneurship </a></li>
                    <li><a href="#" style="color: black">Media </a></li>
                    <li><a href="#" style="color: black">Tech </a></li>
                    <li>   <a href="#" style="color: black">Tutorials </a></li>
                </ul>
                <div class="footer_social"> 
                    <h2>Follow Us</h2>
                    <ul class="media">
                        <li><a class="fab fa-facebook" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-twitter" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-pinterest" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-instagram" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-youtube" href="#" style="color: black"> </a></li>
                    </ul>
                    <ul class="store"> 
                        <li> <a href=""><img src="./img/core-img/app-store.png" alt=""></a></li>
                        <li> <a href=""><img src="./img/core-img/google-play.png" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </section>
        
        <section id="toast"></section>
        <audio id="audio" src="">    </audio>
        <script src="./node_modules/jarallax/dist/jarallax.min.js"></script>
        <script src="./js/app.js"></script>
        <script src="./js/jquery-3.2.1.min.js"></script>
        <script src="./js/handle_ajax.js"></script>
        <script src="./js/handle_toast.js"></script>
    </body>
</html>

