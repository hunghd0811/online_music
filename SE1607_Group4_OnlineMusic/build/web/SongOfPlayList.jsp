<%-- 
    Document   : SongOfPlayList
    Created on : Jun 3, 2022, 2:06:14 AM
    Author     : Black
--%>

<%@page import="java.util.List"%>
<%@page import="model.Songs"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="user" scope="page" value="${sessionScope.user}" />
<c:set var="album1" scope="page" value="${requestScope.album1}" />
<c:set var="album2" scope="page" value="${requestScope.album2}" />
<c:set var="album3" scope="page" value="${requestScope.album3}" />
<c:set var="id" scope="page" value="${requestScope.id}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>JSP Page</title>
        <link rel="stylesheet" href="./css/app.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.1/css/all.css" type="text/css">
    </head>
    <style>
        img.card-img-top{
            width: 90%;
            height: 300px;
        }
        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-left: 60px;
        }
        .col-lg-4 {
            flex: 0 0 33.333333%;
        }
        .btn{
            margin-left: 200px;
            margin-bottom: 30px;
        }
    </style>
    <body>
        <%--Hearder--%>
        <section id="header" style="background: #000000">
            <div class="header-mobile">
                <div class="mobile-toggle"><i class="far fa-times close"></i></div>
                <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                <div class="menu-nav"> 
                    <ul class="header__nav-moblie">
                        <li><a class="current" href="home">Home </a></li>
                        <li><a href="./vpop">Nhạc Việt Nam </a></li>
                        <li><a href="./us-uk">Nhạc Quốc Tế </a></li>
                        <li><a href="./lofi">Lofi </a></li>
                        <li>
                            <%
                                if (session.getAttribute("user") == null) {
                            %> 
                            <br>
                            <a href="login.jsp">Đăng nhập</a>
                            <%
                                }
                            %>
                        </li>
                    </ul>

                    <%--Search--%>
                    <div class="header__search"> 
                        <form value ="txtSearch" action="search" method="post">
                            <input type="text" name="search" placeholder="Search and hit enter...">
                            <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                        </form>
                    </div>
                </div>
            </div>
            <%--Hearder container--%>                
            <div class="header__container container-fluid">
                <div class="header__content"> 
                    <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                    <div class="header__menu"> 
                        <ul class="header__nav">
                            <c:if test="${user.role_ID == 1}">
                                <li><a href="./listacc">Manage Account</a></li>
                                <li><a href="./manager">Manage Songs</a></li>
                                </c:if>
                            <li><a href="./home">Home </a></li>
                            <li><a href="./vpop">Nhạc Việt Nam </a></li>
                            <li><a href="./us-uk">Nhạc Quốc Tế </a></li>
                            <li><a href="./lofi">Lofi </a></li>
                            <li><a href="./albums">Albums </a></li>
                        </ul>
                        <div class="header__search"> 
                            <form value ="txtSearch" action="search" method="post">
                                <input type="text" name="search" placeholder="Search and hit enter...">
                                <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                            </form>
                        </div>
                        <ul class="header__social"> 
                            <li><a class="fab fa-facebook" href="#"> </a></li>
                            <li><a class="fab fa-twitter" href="#"> </a></li>
                            <li><a class="fab fa-youtube" href="#"></a></li>
                            <li class="login">
                                <%
                                    if (session.getAttribute("user") == null) {
                                %>                           
                                <a href="login.jsp">Đăng nhập</a>
                                <%
                                    }
                                %>
                            </li>

                            <c:if test="${user != null}">
                                <li>
                                    <div class="user"> 
                                        <a href="UpdateProfile"><div class="user__avatar bg-img" style="background-image: url('./img/core-img/avatar-default-1.jpg');"></div></a>
                                        <a class="fas fa-sign-out-alt icon" href="signout"></a>
                                        <div class="user__option"> 
                                            <div class="user__option-content"> 
                                                <div class="option-item view-info">
                                                    <div class="user__avatar bg-img" style="background-image: url('./img/core-img/avatar-default-1.jpg');"></div>
                                                    <div class="user__info"> 
                                                        <div class="user__name heading">${user.username} </div>
                                                        <div class="subtitle">See your profile</div>
                                                    </div>
                                                </div>
                                                <div class="option-item setting"><i class="fas fa-cog icon"></i>
                                                    <div class="heading">Settings </div>
                                                </div>
                                                <form action="sign">
                                                    <input type="text" name="url" value="home" hidden>
                                                    <label for="user__sign-out--pc">
                                                        <div class="option-item logout"><i class="fas fa-sign-out-alt icon"></i>
                                                            <div class="heading">Log Out</div>
                                                        </div>
                                                    </label>
                                                    <input type="submit" hidden id="user__sign-out--pc">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </c:if>

                        </ul>
                    </div>
                    <div class="mobile-toggle"><i class="far fa-bars open"></i><i class="far fa-times close">  </i></div>
                </div>
                <div class="header__background"></div>
            </div>
        </section> 

        <%--post songs of album--%>
        <section id="recommend">
            <div class="container">
                <div class="group-tab__content">
                    <div class="tab__container">
                        <div class="tab__heading">
                            <h2>Bài Hát</h2>
                            <div class="btn-group">
                                <div class="btn btn--second btn-addList option"> <i class="fas fa-download"> <span>Tải về</span></i></div>
                                <div class="btn btn-playAll"> <i class="fas fa-play"><span>Phát tất cả</span></i></div>
                            </div>
                        </div>
                        <div class="tab__content vpop-tab active">
                            <div class="container__slide hide-on-mobile">
                                <div class="container__slide-show">
                                    <% int index = 0;%>
                                    <c:forEach var="album" items="${data1}">
                                        <% index++;%>
                                        <div class="container__slide-item <% switch (index) {
                                    case 1: %>first<% break;
                                    case 2: %>second<% break;
                                                        default: %>third<% break;
                                                                }%>">
                                            <div class="container__slide-img" style="background: url('${album.image}') no-repeat center center / cover">
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>

                            <div class="tab__container-list">
                                <div class="play-list scroll-overflow">
                                    <c:forEach var="album" items="${data1}">
                                        <div class="song-item" data-path="${album.path}">
                                            <div class="song-content">
                                                <div class="song-img bg-img" style="background-image: url('${album.image}');">
                                                    <div class="song-img--hover"><i class="fas fa-play"></i></div>
                                                </div>
                                                <div class="song-info"> 
                                                    <h2 class="song-name">${album.name}</h2>
                                                    <div class="song-author">${album.author}</div>
                                                </div>
                                            </div>
                                            <div class="song-duration">${album.duration}</div>
                                            <div class="player__content">
                                                <div class="player__control"> 
                                                    <div class="play"><i class="fas fa-play-circle"></i></div>
                                                    <div class="pause active"><i class="fas fa-pause-circle"></i></div><span class="currenTime">00:00</span>
                                                    <div class="sidebarTime--bg">
                                                        <div class="sidebarTime--current"></div>
                                                    </div><span class="duration">${album.duration}</span>
                                                </div>
                                            </div>
                                            <div class="song-option"> 
                                                <div class="option download"><c:if test="${user != null}"><a href="${album.path}" download></c:if><i class="fal fa-arrow-alt-to-bottom"></i></a></div>
                                                <div <c:if test="${user != null}"> data-user_id="${user.user_ID}" data-album_id="${album.song_ID}"</c:if> class="option like <c:if test="">active</c:if>"> <i class="fas fa-heart"></i></div>
                                                </div>
                                            </div>
                                    </c:forEach>
                                </div>
                            </div>      
                        </div>
                    </div>
                </div>
        </section>

        <%--newletter--%>
        <section id="newletter">
            <div class="jarallax bg-overlay bg-img" style="background-image: url('./img/core-img/jarallax.jpg');">
                <div class="container newletter__container">
                    <div class="newletter__content">
                        <h1>Sign Up To Newsletter</h1>
                        <p>Subscribe to receive info on our latest news and episodes</p>
                    </div>
                    <div class="newletter__subcribe">
                        <form action="" method="method">
                            <input type="text" placeholder="Your Email">
                            <input type="submit" value="SUBCRIBE">
                        </form>
                    </div>
                </div>
            </div>
        </div
    </section>
    <section id="footer">
        <div class="container footer__container">
            <div class="footer__about"> 
                <h2>About Us</h2>
                <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                <p>&copy; Copyright &copy; 2022 <span>Group Four</span></p>
            </div>
            <ul class="footer__categories"> 
                <h2>Categories</h2>
                <li><a href="#">Entrepreneurship </a></li>
                <li><a href="#">Media </a></li>
                <li><a href="#">Tech </a></li>
                <li>   <a href="#">Tutorials </a></li>
            </ul>
            <div class="footer_social"> 
                <h2>Follow Us</h2>
                <ul class="media">
                    <li><a class="fab fa-facebook" href="#"> </a></li>
                    <li><a class="fab fa-twitter" href="#"> </a></li>
                    <li><a class="fab fa-pinterest" href="#"> </a></li>
                    <li><a class="fab fa-instagram" href="#"> </a></li>
                    <li><a class="fab fa-youtube" href="#"> </a></li>
                </ul>
                <ul class="store"> 
                    <li> <a href=""><img src="./img/core-img/app-store.png" alt=""></a></li>
                    <li> <a href=""><img src="./img/core-img/google-play.png" alt=""></a></li>
                </ul>
            </div>
        </div>
    </section>
    <c:if test="${user != null}">
        <section class="bg-overlay" id="user">
            <div class="user-container">
                <div class="user-content">
                    <form action="./user/update?id=${user.user_ID}" method="POST">
                        <input type="text" name="url" value="/home" hidden>
                        <div class="user__base-info"> 
                            <div class="user_avatar bg-img" style="background-image: url('./img/core-img/avatar-default-1.jpg');"></div>
                            <div class="user__fullname"> 
                                <div class="first_name"> 
                                    <div class="title">First name: </div>
                                    <div class="value"> <span class="active">${user.first_name}</span>
                                        <input class="edit_value" type="text" name="first-name" value="${user.first_name}" placeholder="First-name ...">
                                    </div><i class="fas fa-edit icon-setting active"> </i><i class="fal fa-window-close icon-setting-close"></i>
                                </div>
                                <div class="last_name"> 
                                    <div class="title">Last name: </div>
                                    <div class="value"> <span class="active">${user.last_name}</span>
                                        <input class="edit_value" type="text" name="last-name" value="${user.last_name}" placeholder="Last-name ...">
                                    </div><i class="fas fa-edit icon-setting active"> </i><i class="fal fa-window-close icon-setting-close"></i>
                                </div>
                            </div>
                        </div>
                        <div class="user__more-info">
                            <div class="user_username">
                                <div class="title"> <i class="fas fa-user icon"></i><span>Username: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                <div class="value"> 
                                    <div class="edit_value"></div><span class="active">${user.username}</span>
                                </div>
                            </div>
                            <div class="user_password">
                                <div class="title"> <i class="fas fa-key icon"></i><span>Password: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                <div class="value"> <span class="active">********</span>
                                    <div class="edit_password edit_value">
                                        <input type="password" name="old-password" placeholder="Mật khẩu cũ">
                                        <input type="password" name="new-password" placeholder="Mật khẩu mới">
                                        <input type="password" name="verify-password" placeholder="Nhập lại mật khẩu mới">
                                    </div>
                                </div>
                            </div>
                            <div class="user_email"> 
                                <div class="title"> <i class="fas fa-envelope-open icon"></i><span>Email: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                <div class="value"> <span class="active">${user.email}</span>
                                    <input class="edit_value" type="email" name="email" value="${user.email}" placeholder="Email ....">
                                </div>
                            </div>
                            <div class="user_time-create">
                                <div class="title"> <i class="fas fa-calendar-star icon"></i><span>Time create: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                <div class="value"> 
                                    <div class="edit_value"></div><span class="active">${user.t_create}</span>
                                </div>
                            </div>
                        </div>
                        <div class="user__icon-summit"> 
                            <label for="user__edit_summit"><i class="fas fa-check-circle edit-summit"></i></label>
                            <input type="submit" hidden id="user__edit_summit">
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </c:if>
    <section id="toast"></section>
    <audio id="audio" src="">    </audio>
    <script src="./node_modules/jarallax/dist/jarallax.min.js"></script>
    <script src="./js/app.js"></script>
    <script src="./js/jquery-3.2.1.min.js"></script>
    <script src="./js/handle_ajax.js"></script>
    <script src="./js/handle_toast.js"></script>

</body>
</html>


