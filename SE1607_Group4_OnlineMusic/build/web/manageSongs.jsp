<%-- 
    Document   : manageSongs
    Created on : May 29, 2022, 12:38:21 AM
    Author     : Thanh Thao
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Manager Songs</title>

        <!--        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
                <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                <link href="assets/css/manager.css" rel="stylesheet" type="text/css"/>-->
        <!------------------------------------------------------------->
        <!--bs-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/manageSongs.css" rel="stylesheet" type="text/css"/>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
        <!--font awesome-->
        <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
        <style>
            img{
                width: 200px;
                height: 120px;
            }
            .table-title {
                background: gray;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#dtTableProduct').DataTable();
                $('.dataTables_length').addClass('bs-select');
            });
        </script>

    </head>
    <body>

        <form action="deleteSCheckbox" method="post">
            <div class="container">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2>Manage <b>Songs</b></h2>
                                <%
                                    if (session.getAttribute("mess") != null) {
                                %>
                                <h2 id="h2" style="color:greenyellow">${sessionScope.mess}</h2>
                                <%
                                        session.removeAttribute("mess");
                                    }
                                %>  

                                <%
                                    if (session.getAttribute("mess1") != null) {
                                %>
                                <h2 id="h2" style="color:red">${sessionScope.mess1}</h2>
                                <%
                                        session.removeAttribute("mess1");
                                    }
                                %>  

                                <%
                                    if (session.getAttribute("mess2") != null) {
                                %>
                                <h2 id="h2" style="color:gold">${sessionScope.mess2}</h2>
                                <%
                                        session.removeAttribute("mess2");
                                    }
                                %>  
                            </div>



                            <div class="col-sm-6">
                                <a href="#addEmployeeModal"  class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New Product</span></a>

                                <button type="submit" onclick="return confirm('You want to delete those product?');" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></button>						
                            </div>

                        </div>
                    </div>

                    <table id="dtTableProduct"  class="table table-striped table-hover ">
                        <thead>
                            <tr>
                                <th>
                                    <span class="custom-checkbox">
                                        <input type="checkbox"   id="selectAll">
                                        <label for="selectAll"></label>
                                    </span>  
                                </th>

                                <th>ID</th>
                                <th>Category</th>
                                <th>Name</th>
                                <th>Author</th>
                                <th>Image</th>
                                <th>Path</th>
                                <th>Duration</th>
                                <th>Time Create</th>
                                <th>Time Update</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.listS}" var="o">
                                <tr>

                                    <td>
                                        <span class="custom-checkbox">
                                            <input type="checkbox" id="checkbox1" name="music" value="${o.song_ID}">
                                            <label for="checkbox1"></label>
                                        </span>
                                    </td>  
                                    <td>${o.song_ID}</td>
                                    <td>
                                        <c:forEach var="C" items="${listC}">
                                            <c:if test="${C.categoryID == o.category_ID}">
                                                ${C.categoryDetail_name}<br>
                                            </c:if>
                                        </c:forEach>
                                    </td>

                                    <td>
                                        <a href= "songDetail?id=${o.song_ID}">${o.name}</a>
                                    </td>
                                    <td>${o.author}</td>
                                    <td>
                                        <img src="${o.image}">
                                    </td>
                                    <td>${o.path}</td>
                                    <td>${o.duration}</td>
                                    <td>${o.t_create}</td>
                                    <td>${o.t_lastUpdate}</td>
                                    <td>
                                        <a href="update?id=${o.song_ID}"  class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                        <a href="delete?id=${o.song_ID}" onclick="return confirm('You want to delete this product?');" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>

                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <a href="home"><button type="button" class="btn btn-danger">Back to home</button></a>
            </div>
        </form>         

    </div>
    <!-- Edit Modal HTML -->
    <div id="addEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="add" method="get">
                    <div class="modal-header">						
                        <h4 class="modal-title">Add Songs</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">					
                        <div class="form-group">
                            <label>Name*</label>
                            <input name="name" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Author</label>
                            <input name="author" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Image</label>
                            <input name="image" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Path Songs*</label>
                            <input name="path" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Duration</label>
                            <input name="duration" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Lyric</label>
                            <textarea name="lyric" type="text" class="form-control" required ></textarea>
                        </div> 
                        <div class="form-group">
                            <label>Category</label>
                            <select name="category"  class="form-select" aria-label="Default select example">
                                <option value="0">Non-Category</option>
                                <c:forEach items="${requestScope.listC}" var="o">
                                    <option value="${o.categoryID}">${o.categoryDetail_name}</option>
                                </c:forEach>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <input type="submit" class="btn btn-success" value="Add">
                    </div>
                </form>
            </div>
        </div>
    </div>




    <script src="js/manager.js" type="text/javascript"></script>
</body>
</html>
