<%-- 
    Document   : resetPassword
    Created on : Apr 14, 2022, 3:56:39 PM
    Author     : trinh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Loustic</title>
        <meta name="description" content="Bonx is a terrific esports website template with a slick and modern look.  It’s a robust gaming HTML template for bloggers and online gamers who want to share their enthusiasm for games on the internet."/>
        <meta name="keywords" content="	bootstrap, clean, esports, game, game portal, Game website, gamer, games, gaming, magazine, match, modern, online game, sport, sports">
        <meta name="author" content="Code Carnival">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Add site Favicon -->
      

        <!-- CSS 
        ========================= -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Exo:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Metal+Mania&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/slick.css">
        <link rel="stylesheet" href="assets/css/icofont.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/nice-select.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <!-- Main Style CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
              <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        
    </style>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    </head>
    <body class="body__bg" data-bgimg="assets/img/bg/body-bg.webp">
    

        <!-- breadcrumbs area start -->
        <div class="breadcrumbs_aree breadcrumbs_bg mb-140" data-bgimg="assets/img/bg/breadcrumbs-bg.webp">          
        </div>
        <!-- breadcrumbs area end -->
        <!-- page wrapper start -->
        <div class="page_wrapper">

            <!-- contact section start -->
            <section class="contact_page_section mb-140">
                <div class="container">
                    <div class="row justify-content-between align-items-center mb-n50">
                        <div class="col-lg-6 col-md-8 col-12 mx-auto mb-50">
                            <img width="550px" height="550px" src="assets/img/others/undraw_Problem_solving_re_4gq3.png" style="border-radius: 10px" alt="">
                        </div>
                        <div class="col-lg-5 col-md-8 col-12 mx-auto mb-50">
                            <div class="section_title text-center mb-60">
                                <h2>Reset password</h2>
                            </div>
                            <form method="post" action="changepass">
                                <div class="form_input">
                                    <input name="id" type="hidden" value="${requestScope.user.user_ID}">
                                </div>  
                                 <div class="form_input">
                                    <input name="random" type="hidden" value="${requestScope.user.random}">
                                </div>
                                <div class="form_input">
                                    <input readonly name="email" type="email" value="${requestScope.user.email}" >
                                </div>
                                
                                <div class="form_input">
                                    <input name="npassword" id="password1" placeholder="New password" type="password" required="" >
                                </div>
                                <div class="form_input">
                                    <input name="repassword" id="password2" placeholder="Retype new password" type="password" required="">
                                </div>
                                
                            <div class="col-sm-12">
                                <span id="pwmatch" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Passwords Match
                            </div>
                        
                                <c:choose>
                                    <c:when test="${error.equals('1')}">
                                        <div class="alert alert-danger" role="alert">
                                            Re-password not match
                                        </div>
                                    </c:when>
                                    <c:when test="${error.equals('2')}">
                                        <div class="alert alert-danger" role="alert">
                                            Some error, try again!
                                        </div>
                                    </c:when>                                
                                    <c:otherwise>

                                    </c:otherwise>
                                </c:choose>
                                <div class="form_input_btn text-center mb-40">
                                    <button type="submit" class="btn btn-link">Change<img width="20" height="20" src="assets/img/icon/arrrow-icon.webp" alt=""></button>
                                </div>
                            </form>
                            <p class="text-center">Remembered password, <a href="${pageContext.request.contextPath}/sign">Login here</a></p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- contact section end -->

        </div>
        <!-- page wrapper end -->

       
        <!--modernizr min js here-->
        <script src="assets/js/vendor/modernizr-3.7.1.min.js"></script>

        <!-- Vendor JS -->
        <script src="assets/js/vendor/jquery-3.6.0.min.js"></script>
        <script src="assets/js/vendor/jquery-migrate-3.3.2.min.js"></script>
        <script src="assets/js/vendor/popper.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/slick.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nice-select.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/js/jquery.counterup.min.js"></script>
        <script src="assets/js/jquery-waypoints.js"></script>

        <!-- Main JS -->
        <script src="assets/js/main.js"></script>
        <script type="text/javascript">
            $("input[type=password]").keyup(function () {


                if ($("#password1").val() == $("#password2").val()) {
                    $("#pwmatch").removeClass("glyphicon-remove");
                    $("#pwmatch").addClass("glyphicon-ok");
                    $("#pwmatch").css("color", "#00A41E");
                } else {
                    $("#pwmatch").removeClass("glyphicon-ok");
                    $("#pwmatch").addClass("glyphicon-remove");
                    $("#pwmatch").css("color", "#FF0004");
                }
            });
        </script>
    </body>
</html>
