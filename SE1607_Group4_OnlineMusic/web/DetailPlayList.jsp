
<%@page import="java.util.List"%>
<%@page import="model.Songs"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : home
    Created on : 28-05-2022
    Author     : duy
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="user" scope="page" value="${sessionScope.user}" />
<c:set var="album1" scope="page" value="${requestScope.album1}" />
<c:set var="album2" scope="page" value="${requestScope.album2}" />
<c:set var="album3" scope="page" value="${requestScope.album3}" />
<c:set var="id" scope="page" value="${requestScope.id}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>PlayList Song</title>
        <link href="css/manageSongs.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="./css/app.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.1/css/all.css" type="text/css">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    </head>
    <style >
        .btn {
            position: relative;
            z-index: 1;
            height: 46px;
            line-height: 43px;
            font-size: 14px;
            font-weight: 600;
            display: inline-block;
            padding: 0 20px;
            text-align: center;
            text-transform: uppercase;
            border-radius: 30px;
            -webkit-transition: all 500ms;
            -o-transition: all 500ms;
            transition: all 500ms;
            border: 2px solid #f55656;
            letter-spacing: 1px;
            box-shadow: none;
            background-color: #f55656;
            color: #ffffff;
            cursor: pointer;
        }
    </style>
    <body>
        <section id="header" style="background: #000">
            <div class="header-mobile">
                <div class="mobile-toggle"><i class="far fa-times close"></i></div>
                <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                <div class="menu-nav"> 
                    <ul class="header__nav-moblie">
                        <li>
                            <%
                                if (session.getAttribute("user") == null) {
                            %> 
                            <br>
                            <a href="login.jsp">Đăng nhập</a>
                            <%
                                }
                            %>
                        </li>
                    </ul>

                    <%--Search--%>
                    <div class="header__search"> 
                        <form value="txtSearch" action="search" method="post">
                            <input type="text" name="search" placeholder="Search and hit enter...">
                            <select name="searchType">
                                <option value="1">Search By Name</option>
                                <option value="2">Search By Lyrics</option>
                                <option value="3">Search By Author</option>

                            </select>
                            <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                        </form>
                    </div>
                </div>
            </div>
            <%--Hearder container--%>                
            <div class="header__container container-fluid">
                <div class="header__content" style="background-color: #000"> 
                    <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                    <div class="header__menu"> 
                        <ul class="header__nav">
                            <c:if test="${user.role_ID == 1}">
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="./listacc">Manage Account</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="./manageAlbum">Manage Albums</a></li>
                                        <li><a class="dropdown-item" href="./manager">Manage Songs</a></li>
                                        <li><a class="dropdown-item" href="./managecomment">Manage Comment</a></li>
                                        <li><a class="dropdown-item" href="managehistory">Manage View History</a></li>
                                    </ul>
                                </li>
                            </c:if>
                            <c:if test="${user.role_ID == 3}">
                                <li><a href="./manager">Manage Songs</a></li>
                                </c:if>    
                            <li><a class="current" href="./home">Home </a></li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${1}">Nhạc Việt Nam </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${1}"> Nhạc Trẻ</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${2}"> Nhạc Trữ Tình </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${3}"> Nhạc Cách Mạng </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${2}">Nhạc Quốc Tế </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${4}"> Nhạc US-UK</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${5}"> Nhạc KPOP </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${6}"> Nhạc Anime </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${3}">Lofi </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${8}"> Nhạc Piano</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${9}"> Nhạc Guitar </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${7}"> Nhạc Chill </a></li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link" href="./albums">Albums </a>

                                <c:if test="${user != null}">
                                <li class="nav-item dropdown"><a class="nav-link" href="./allplaylist">View PlayList </a></li>                                
                                </c:if>
                            </li>
                            <c:if test="${user != null}">
                                <li class="nav-item dropdown"><a class="nav-link" href="history">Recently Visited</a></li>
                                </c:if>

                        </ul>
                        <div class="header__search"> 
                            <form value ="txtSearch" action="search" method="post" >
                                <input type="text" name="txtsearch" placeholder="Search and hit enter...">
                                <select style="background-color: #616161; color: #ffffff; border-radius: 5px"  name="searchType">
                                    <option value="1">Search By Name</option>
                                    <option value="2">Search By Lyrics</option>
                                    <option value="3">Search By Author</option>

                                </select>
                                <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                            </form>
                        </div>
                        <ul class="header__social">      
                            <li class="login">
                                <%
                                    if (session.getAttribute("user") == null) {
                                %>                           
                                <a href="login.jsp">Đăng nhập</a>
                                <%
                                    }
                                %>
                            </li>

                            <c:if test="${user != null}">
                                <li>
                                    <div class="user"> 
                                        <a href="UpdateProfile"><div class="user__avatar bg-img" 
                                                                     <c:if test="${(user.avatar == null) or (user.avatar.isEmpty())}">
                                                                         style="background-image: url('./img/avatar.jpg');"
                                                                     </c:if>
                                                                     <c:if test="${user.avatar != null}">
                                                                         style="background-image: url('./img/uploads/${user.avatar}');"
                                                                     </c:if>
                                                                     ></div></a>
                                        <a class="fas fa-sign-out-alt icon" href="signout"></a>
                                        <div class="user__option"> 
                                            <div class="user__option-content"> 
                                                <div class="option-item view-info">
                                                    <div class="user__avatar bg-img" style="background-image: url('./img/core-img/avatar-default-1.jpg');"></div>
                                                    <div class="user__info"> 
                                                        <div class="user__name heading">${user.username} </div>
                                                        <div class="subtitle">See your profile</div>
                                                    </div>
                                                </div>
                                                <div class="option-item setting"><i class="fas fa-cog icon"></i>
                                                    <div class="heading">Settings </div>
                                                </div>
                                                <form action="sign">
                                                    <input type="text" name="url" value="home" hidden>
                                                    <label for="user__sign-out--pc">
                                                        <div class="option-item logout"><i class="fas fa-sign-out-alt icon"></i>
                                                            <div class="heading">Log Out</div>
                                                        </div>
                                                    </label>
                                                    <input type="submit" hidden id="user__sign-out--pc">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </c:if>

                        </ul>
                    </div>
                    <div class="mobile-toggle"><i class="far fa-bars open"></i><i class="far fa-times close">  </i></div>
                </div>
                <div class="header__background"></div>
            </div>
        </section>  

        <%--post songs of album--%>
        <section id="recommend">
            <div class="container">
                <div class="group-tab__content">
                    <div class="tab__container">
                        <div class="tab__heading">
                            <h2>Bài Hát</h2>

                        </div>
                        <div class="tab__content vpop-tab active">
                            <div class="tab__container-list">
                                <div class="play-list scroll-overflow">
                                    <c:forEach var="o" items="${requestScope.detailPlaylist}">
                                        <div class="song-item" data-path="${o.path}">
                                            <div class="song-content">
                                                <div class="song-img bg-img" style="background-image: url('${o.image}');">
                                                    <div class="song-img--hover"><i class="fas fa-play"></i></div>
                                                </div>
                                                <div class="song-info"> 
                                                    <h2 class="song-name">${o.name}</h2>
                                                    <div class="song-author">${o.author}</div>
                                                </div>
                                            </div>
                                            <div class="song-duration">${o.duration}</div>
                                            <div class="player__content">
                                                <div class="player__control"> 
                                                    <div class="play"><i class="fas fa-play-circle"></i></div>
                                                    <div class="pause active"><i class="fas fa-pause-circle"></i></div><span class="currenTime">00:00</span>
                                                    <div class="sidebarTime--bg">
                                                        <div class="sidebarTime--current"></div>
                                                    </div><span class="duration">${o.duration}</span>
                                                </div>
                                            </div>

                                        </div>
                                    </c:forEach>
                                </div>
                            </div>      
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <%--newletter--%>
        <section id="newletter">
            <div class="jarallax bg-overlay bg-img" style="background-image: url('./img/core-img/jarallax.jpg');">
                <div class="container newletter__container">
                    <div class="newletter__content">
                        <h1>Sign Up To Newsletter</h1>
                        <p>Subscribe to receive info on our latest news and episodes</p>
                    </div>
                    <div class="newletter__subcribe">
                        <form action="" method="method">
                            <input type="text" placeholder="Your Email">
                            <input type="submit" value="SUBCRIBE">
                        </form>
                    </div>
                </div>
            </div>
        </div
    </section>
    <section id="footer">
            <div class="container footer__container" style="display: flex">
                <div class="footer__about"> 
                    <h2>About Us</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                    <p>&copy; Copyright &copy; 2022 <span>Group Four</span></p>
                </div>
                <ul class="footer__categories"> 
                    <h2>Categories</h2>
                    <li><a href="#" style="color: black">Entrepreneurship </a></li>
                    <li><a href="#" style="color: black">Media </a></li>
                    <li><a href="#" style="color: black">Tech </a></li>
                    <li>   <a href="#" style="color: black">Tutorials </a></li>
                </ul>
                <div class="footer_social"> 
                    <h2>Follow Us</h2>
                    <ul class="media">
                        <li><a class="fab fa-facebook" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-twitter" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-pinterest" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-instagram" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-youtube" href="#" style="color: black"> </a></li>
                    </ul>
                    <ul class="store"> 
                        <li> <a href=""><img src="./img/core-img/app-store.png" alt=""></a></li>
                        <li> <a href=""><img src="./img/core-img/google-play.png" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </section>
    
    <section id="toast"></section>
    <audio id="audio" src="">    </audio>
    <script src="./node_modules/jarallax/dist/jarallax.min.js"></script>
    <script src="./js/app.js"></script>
    <script src="./js/jquery-3.2.1.min.js"></script>
    <script src="./js/handle_ajax.js"></script>
    <script src="./js/handle_toast.js"></script>

</body>
</html>


