<%-- 
    Document   : managealbum
    Created on : May 31, 2022, 9:36:24 PM
    Author     : anhqu
--%>




<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Manager Albums</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/manage.css" rel="stylesheet" type="text/css"/>
        
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

        <style>
            img{
                width: 200px;
                height: 120px;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#dtTableProduct').DataTable();
                $('.dataTables_length').addClass('bs-select');
            });
        </script>
    </head>
    <body>
        <form action="updatealbum" method="post">
        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Manage <b>Albums</b></h2>
                        </div>
                        
                        <div class="col-sm-6">                  
                            <button type="submit" class="btn btn-success" data-toggle="modal"> <i class="material-icons" >&#xE147;</i> <span>Add New Song to Album</span></button>                                        
                        </div>
                      
                    </div>
                </div>
                <table id="dtTableProduct" class="table table-striped table-hover ">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Category</th>
                            <th>Name</th>
                            <th>Author</th>
                            <th>Image</th>                         
                            <th>Duration</th>                           
                            <th>Actions</th>
                            
                        </tr>
                    </thead>
                    <tbody>                   
                        <c:forEach items="${requestScope.listS}" var="o">
                            <tr>
                                <td>${o.song_ID}</td>
                               <td>
                                <c:forEach var="C" items="${listC}">
                                     <c:if test="${C.categoryID == o.category_ID}">
                                        ${C.categoryDetail_name}<br>
                                    </c:if>
                                </c:forEach>
                                </td>
                                <td>${o.name}</td>
                                <td>${o.author}</td>
                                <td>
                                    <img src="${o.image}">
                                </td>                             
                                 <td>${o.duration}</td> 
                      
                                 <td>                                     
                                    <input type="checkbox" name="music" value="${o.song_ID}">    
                                   <input type="hidden" name="albumid" value="${albumid}">
                                 </td>
                                 
                                                          
                            </tr>
                      </c:forEach>
                    </tbody>
                </table>
            </div>
          
                                     
            <a href="manageAlbum"><button type="button" class="btn btn-primary">Back to Albums</button></a>

        </div>
         </form>

        <script src="js/manager.js" type="text/javascript"></script>
    </body>
</html>