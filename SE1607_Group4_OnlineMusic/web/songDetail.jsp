<%-- 
    Document   : songDetail
    Created on : Jun 5, 2022, 6:05:16 PM
    Author     : Auriat
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="user" scope="page" value="${sessionScope.user}" />
<c:set var="sliders" scope="page" value="${requestScope.sliders}" />
<c:set var="albums_Vpop" scope="page" value="${requestScope.albums_Vpop}" />
<c:set var="albums_USUK" scope="page" value="${requestScope.albums_USUK}" />
<c:set var="albums_Lofi" scope="page" value="${requestScope.albums_Lofi}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Song</title>
        <link rel="stylesheet" href="./css/app.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.1/css/all.css" type="text/css">
        <meta charset="utf-8">
        <!--  This file has been downloaded from bootdey.com @bootdey on twitter -->
        <!--  All snippets are MIT license http://bootdey.com/license -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Calibri:400,300,700" type="text/css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <title>Loustic</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <style>
            @media all and (min-width: 992px) {
                .header__nav .nav-item .dropdown-menu{ display: none; }
                .header__nav .nav-item:hover .nav-link{   }
                .header__nav .nav-item:hover .dropdown-menu{ display: block; background-color: #e2264d }
                .header__nav .nav-item .dropdown-menu{ margin-top:0; }
            }
            .slider__item .slider__content {
                margin-bottom: 500px;
            }
            .bg-overlay::after {
                position: absolute;
                content: "";
                height: 100%;
                width: 100%;
                top: 0;
                left: 0;
                z-index: -1;
            }

            [id="heart"] {
                position: absolute;
                left: -100vw;
            }

            [for="heart"] {
                color: #aab8c2;
                cursor: pointer;
                font-size: 17px;
                align-self: center;  
                transition: color 0.2s ease-in-out;
            }

            [for="heart"]:hover {
                color: grey;
            }

            [for="heart"]::selection {
                color: none;
                background: transparent;
            }

            [for="heart"]::moz-selection {
                color: none;
                background: transparent;
            }


            [id="heart"]:checked + label {
                color: #e2264d;
                will-change: font-size;
                animation: heart 1s cubic-bezier(.17, .89, .32, 1.49);
            }

            .lyric.active {
                color: yellow;
                font-size: 20px;
            }

            @keyframes heart {0%, 17.5% {font-size: 0;}}
            select{
                background-color: #616161;
                color: white;
                border-radius: 10px;
            }
            body{font-family: 'Calibri', sans-serif !important}
            .mt-100{margin-top: 100px}
            .mb-100{margin-bottom:100px}
            .card{position: relative;display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-orient: vertical;-webkit-box-direction: normal;-ms-flex-direction: column;flex-direction: column;min-width: 0;word-wrap: break-word;background-color: #fff;background-clip: border-box;border: 0px solid transparent;border-radius: 0px}
            .card-body{-webkit-box-flex: 1;-ms-flex: 1 1 auto;flex: 1 1 auto;padding: 1.25rem}
            .card .card-title{position: relative;font-weight: 600;margin-bottom: 10px}
            .comment-widgets{position: relative;margin-bottom: 10px}
            .comment-widgets .comment-row{border-bottom: 1px solid transparent;padding: 14px;display: -webkit-box;display: -ms-flexbox;display: flex;margin: 10px 0}
            .p-2{padding: 0.5rem !important}
            .comment-text{padding-left: 15px}
            .w-100{width: 100% !important}
            .m-b-15{margin-bottom: 15px}
            .btn-sm{padding: 0.25rem 0.5rem;font-size: 0.9rem;line-height: 1.5;border-radius: 2px; height: 35px}
            .btn-cyan{color: #fff;background-color: #27a9e3;border-color: #27a9e3}
            .btn-cyan:hover{color: #fff;background-color: #1a93ca;border-color: #198bbe}
            .comment-widgets .comment-row:hover{background: rgba(0, 0, 0, 0.05)}
            .float-right {float: right !important; }
            .float-left {float: left !important; }
            .font-medium{font-size: 2rem}
        </style>
    </head>

    <body>
        <%--Hearder--%>
        <section id="header" style="background: #000">
            <div class="header-mobile">
                <div class="mobile-toggle"><i class="far fa-times close"></i></div>
                <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                <div class="menu-nav"> 
                    <ul class="header__nav-moblie">
                        <li>
                            <%
                                if (session.getAttribute("user") == null) {
                            %> 
                            <br>
                            <a href="login.jsp">Đăng nhập</a>
                            <%
                                }
                            %>
                        </li>
                    </ul>
                    <%--Search--%>
                    <div class="header__search"> 
                        <form value="txtSearch" action="search" method="post" style="display : flex;">
                            <input type="text" name="search" placeholder="Search and hit enter..." style="margin-top: 10px">
                            <select name="searchType" style="height: 30px; margin-top: 12px;">
                                <option value="1">Search By Name</option>
                                <option value="2">Search By Lyrics</option>
                                <option value="3">Search By Author</option>

                            </select>
                            <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                        </form>
                    </div>
                </div>
            </div>
            <%--Hearder container--%>                
            <div class="header__container container-fluid">
                <div class="header__content" style="background-color: #000"> 
                    <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                    <div class="header__menu"> 
                        <ul class="header__nav">
                            <c:if test="${user.role_ID == 1}">
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="./listacc">Manage Account </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="./manageAlbum">Manage Albums</a></li>
                                        <li><a class="dropdown-item" href="./manager">Manage Songs</a></li>
                                        <li><a class="dropdown-item" href="./managecomment">Manage Comment</a></li>
                                        <li><a class="dropdown-item" href="managehistory">Manage View History</a></li>
                                    </ul>
                                </li>
                            </c:if>
                            <c:if test="${user.role_ID == 3}">
                                <li class="nav-item dropdown"><a class="nav-link" href="./manager">Manage Songs</a></li>
                                </c:if>    
                            <li class="nav-item dropdown"><a class="nav-link" href="./home">Home</a></li>                          
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${1}">Nhạc Việt Nam </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${1}"> Nhạc Trẻ</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${2}"> Nhạc Trữ Tình </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${3}"> Nhạc Cách Mạng </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${2}">Nhạc Quốc Tế </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${4}"> Nhạc US-UK</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${5}"> Nhạc KPOP </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${6}"> Nhạc Anime </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${3}">Lofi </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${8}"> Nhạc Piano</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${9}"> Nhạc Guitar </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${7}"> Nhạc Chill </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="./albums">Albums </a>

                                <c:if test="${user != null}">
                                <li class="nav-item dropdown"><a class="nav-link" href="./allplaylist">View PlayList </a></li>                                
                                </c:if>
                            </li>
                            <c:if test="${user != null}">
                                <li class="nav-item dropdown"><a class="nav-link" href="history">Recently Visited</a></li>
                                </c:if>

                        </ul>
                        <div class="header__search"> 
                            <form value ="txtSearch" action="search" method="post" style="display: flex">
                                <input type="text" name="txtsearch" placeholder="Search and hit enter..." style="margin-top: 10px">
                                <select name="searchType" style="height: 30px; margin-top: 12px">
                                    <option value="1">Search By Name</option>
                                    <option value="2">Search By Lyrics</option>
                                    <option value="3">Search By Author</option>

                                </select>
                                <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                            </form>
                        </div>
                        <ul class="header__social">      
                            <li class="login">
                                <%
                                    if (session.getAttribute("user") == null) {
                                %>                           
                                <a href="login.jsp">Đăng nhập</a>
                                <%
                                    }
                                %>
                            </li>
                            <c:if test="${user != null}">
                                <li>
                                    <div class="user"> 
                                        <a href="UpdateProfile"><div class="user__avatar bg-img" 
                                                                     <c:if test="${(user.avatar == null) or (user.avatar.isEmpty())}">
                                                                         style="background-image: url('./img/avatar.jpg');"
                                                                     </c:if>
                                                                     <c:if test="${user.avatar != null}">
                                                                         style="background-image: url('./img/uploads/${user.avatar}');"
                                                                     </c:if>
                                                                     ></div></a>
                                        <a class="fas fa-sign-out-alt icon" href="signout"></a>
                                        <div class="user__option"> 
                                            <div class="user__option-content"> 
                                                <div class="option-item view-info">
                                                    <div class="user__avatar bg-img" style="background-image: url('./img/core-img/avatar-default-1.jpg');"></div>
                                                    <div class="user__info"> 
                                                        <div class="user__name heading">${user.username} </div>
                                                        <div class="subtitle">See your profile</div>
                                                    </div>
                                                </div>
                                                <div class="option-item setting"><i class="fas fa-cog icon"></i>
                                                    <div class="heading">Settings </div>
                                                </div>
                                                <form action="sign">
                                                    <input type="text" name="url" value="home" hidden>
                                                    <label for="user__sign-out--pc">
                                                        <div class="option-item logout"><i class="fas fa-sign-out-alt icon"></i>
                                                            <div class="heading">Log Out</div>
                                                        </div>
                                                    </label>
                                                    <input type="submit" hidden id="user__sign-out--pc">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </c:if>
                        </ul>
                    </div>
                    <div class="mobile-toggle"><i class="far fa-bars open"></i><i class="far fa-times close">  </i></div>
                </div>
                <div class="header__background"></div>
            </div>
        </section>      
        <section id="slider">
            <div class="slider__item bg-overlay bg-img slider__item-1 active" style="background-image: url('${requestScope.slider.image}');">
                <div class="container" style="margin-top: 150px;">
                    <div class="slider__content">
                        <div class="slider__wellcome"> 
                            <div class="slider__group--btn">
                                <form>
                                    <input type="button" class="btn filter" value="Back" onclick="history.back()">
                                </form>
                            </div>
                        </div>
                        <div class="slider__player player song-item" data-path="${requestScope.slider.path}" style="margin-top: -170px;">
                            <!--                            max-width: 90%; max-height: 150px"-->
                            <div class="player__img bg-img" style="background-image: url('${requestScope.slider.image}');">
                                <div class="song-img--hover"></div>
                            </div>
                            <div class="player__content">
                                <div class="player__info">
                                    <p class="player__date">${requestScope.slider.t_create}</p>
                                    <h1 class="player__name song-name">${requestScope.slider.name}</h1>
                                    <p class="player__text"><span class="player__author">${requestScope.slider.author} | </span><span class="player_duration">00:${requestScope.slider.duration}</span></p>
                                </div>
                                <div class="player__control">
                                    <div class="play"><i class="fas fa-play-circle"></i></div>
                                    <div class="pause active"><i class="fas fa-pause-circle"></i></div><span class="currenTime">00:00</span>
                                    <div class="sidebarTime--bg">
                                        <div class="sidebarTime--current"> </div>
                                    </div><span class="duration">${requestScope.slider.duration}</span>
                                    <div class="volume">
                                        <div class="volume__icon"> <i class="mute fas fa-volume-slash"></i><i class="volume--low fas fa-volume active"></i><i class="volume--hight fas fa-volume-up"></i></div>
                                    </div>
                                    <div class="volume__silebar--bg">
                                        <div class="volume__silebar--current"></div>
                                    </div>
                                </div>
                                <div class="like-share-download">
                                    <div class="option like">

                                        <form action="manageLike" method="POST">

                                            <input id="heart" type="checkbox" name="like" onClick="this.form.submit()" ${requestScope.checkerviet == true?"checked":""}/>
                                            <label for="heart">❤</label>
                                            <span>Like</span>

                                            <input value="${requestScope.slider.song_ID}" hidden="true" name="songID" />
                                        </form>
                                        <i class="far fa-eye"></i> ${requestScope.slider.views}<br/>
                                        <c:if test="${user != null}">
                                            <c:if test="${userTotalViews < 2}">
                                                You have visited this song ${userTotalViews} time
                                            </c:if>
                                            <c:if test="${userTotalViews > 1}">
                                                You have visited this song ${userTotalViews} times
                                            </c:if>
                                        </c:if>
                                    </div>
                                    <div class="div">
                                        <div class="option share"> <i class="fas fa-share-alt"> <span>Share</span></i></div>
                                        <div class="option download" ><c:if test="${user != null}"><a href="${requestScope.slider.path}" style="color: black" download></c:if><i class="fas fa-download"> <span>Download</span></i></a></div>
                                        <c:if test="${user != null}">
                                            <c:if test="${userLastVisited != null}">
                                                <br/><br/>Last visited: ${userLastVisited}
                                            </c:if>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="lyrics" style="margin-left:300px; color: white">
                            <div class="btn btn-primary btn-more" style="margin-left: 600px;text-decoration: underline; background: none; border: none" >▼Show more</div>                           
                             <c:forEach var="i" begin="0" end="${lyrics.size()}">
                                <p class="lyric ${i >= 7 ? "d-none" : ""} ${i == 0 ? "active" : ""}" data-time="${lyrics[i].time}">${lyrics[i].content}</p>
                            </c:forEach>
                            <div class="btn btn-primary btn-less d-none" style="margin-left: 600px;text-decoration: underline ;background: none; border: none">▲Show less</div>                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body text-center">
                            <h4 class="card-title">Latest Comments</h4>
                        </div>
                        <div class="comment-widgets">
                            <!-- Comment Row -->
                            <form action="addcomment" method="get">
                                <input type="hidden" name="id" value="${requestScope.slider.song_ID}">
                                <div class="d-flex flex-row comment-row m-t-0">
                                    <c:if test="${(user.avatar == null) or (user.avatar.isEmpty())}">
                                        <img class="img-fluid img-responsive rounded-circle mr-2" src="./img/avatar.jpg" alt="" width="50" class="rounded-circle">
                                    </c:if>
                                    <c:if test="${user.avatar != null}">
                                        <img class="img-fluid img-responsive rounded-circle mr-2" src="./img/uploads/${user.avatar}" alt="" width="50" class="rounded-circle">
                                    </c:if>
                                    <div class="comment-text w-100">
                                        <input type="text" name="comment" class="form-control mr-3" placeholder="Add comment">
                                    </div>
                                    <button class="btn btn-cyan btn-sm" type="submit">Add Comment</button>
                                </div>
                            </form>

                            <c:forEach items="${cmm}" var="cm">
                                <c:if test="${cm.status == true}">
                                    <div class="d-flex flex-row comment-row m-t-0">
                                        <c:if test="${(user.avatar == null) or (user.avatar.isEmpty())}">
                                            <img class="p-2 rounded-circle " src="./img/avatar.jpg" alt="" style="width: 60px; height: 60px">
                                        </c:if>
                                        <c:if test="${user.avatar != null}">
                                            <img class="p-2 rounded-circle" src="./img/uploads/${user.avatar}" alt="" style="width: 60px; height: 60px">
                                        </c:if>                                      
                                        <div class="comment-text w-100">
                                            <c:forEach items="${requestScope.user1}" var="a">
                                                <c:if test="${a.user_ID == cm.userid}">
                                                    <h6 class="font-medium">${a.last_name}</h6>
                                                </c:if>
                                            </c:forEach>
                                            <form id="myForm" action="edicomment" method="get">
                                                <input name="comment" class="m-b-15 d-block" value="${cm.comment}" type="text">
                                                <input value="${cm.commentid}" hidden="true" name="commentID" />
                                                <input value="${cm.songid}" hidden="true" name="songID" />
                                                <input value="${cm.userid}" hidden="true" name="userID" />
                                            </form>
                                            <div class="comment-footer">
                                                    <c:if test="${cm.userid == sessionScope.user.user_ID}"> 
                                                        <a class="" onclick="myFunction()">Edit</a>
                                                        <a class="" href="deletecomment?id=${cm.commentid}&songid=${cm.songid}">Delete</a>
                                                    </c:if>
                                                <a href="report?id=${cm.commentid}&songid=${cm.songid}">Report</a>
                                                <a class="fas fa-heart" 
                                                   <c:forEach items="${requestScope.likeCheck}" var="l">
                                                       <c:if test="${l.comment_ID == cm.commentid}" > style="color: #D32F2F"  </c:if>
                                                   </c:forEach> >
                                                </a>
                                                <a href="manageLike?commentID=${cm.commentid}&songID=${cm.songid}&flag=&userid=${sessionScope.user.user_ID}&operation=likecomment">Like</a>
                                                <a href="manageLike?commentID=${cm.commentid}&songID=${cm.songid}&flag=0&userid=${sessionScope.user.user_ID}&operation=likecomment">Unlike</a>
                                                <span class="text-muted">${cm.tcreate}</span>
                                                <span class="text-muted float-right">${cm.tlastupdate}</span>
                                            </div>

                                            

                                            <div style="padding-left: 5%" >
                                                <c:forEach items="${requestScope.reply}" var="rcm">
                                                    <c:if test="${rcm.comment_ID == cm.commentid}">
                                                        <div class="d-flex flex-row comment-row m-t-0">
                                                            <c:if test="${(user.avatar == null) or (user.avatar.isEmpty())}">
                                                                <img class="p-2 rounded-circle" src="./img/avatar.jpg" alt="" style="width: 60px; height: 60px">
                                                            </c:if>
                                                            <c:if test="${user.avatar != null}">
                                                                <img class="p-2 rounded-circle" src="./img/uploads/${user.avatar}" alt="" style="width: 60px; height: 60px">
                                                            </c:if>
                                                            <div class="comment-text w-100">

                                                                <c:forEach items="${requestScope.user1}" var="a">
                                                                    <c:if test="${a.user_ID == cm.userid}">
                                                                        <h6 class="font-medium">${a.last_name}</h6>
                                                                    </c:if>
                                                                </c:forEach>
                                                                <div>
                                                                    <p>${rcm.content}</p>
                                                                    <ul class="list-unstyled list-inline media-detail pull-left">
                                                                        <li><i class=""></i>${rcm.t_create}</li>
                                                                    </ul>
                                                                    <ul class="list-unstyled list-inline media-detail pull-right">
                                                                        <c:if test="${rcm.user_ID == sessionScope.user.user_ID}">
                                                                            <li class=""><a href="reply?replyID=${rcm.id}&songID=${cm.songid}">Delete</a></li>
                                                                            </c:if>
                                                                    </ul>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </c:forEach>
                                                <div id="replyform-${cm.commentid}">
                                                    <form action="reply" method="POST" >
                                                        <h3 class="pull-left">Reply</h3> <br> <br>
                                                        <div class="d-flex flex-row comment-row m-t-0">
                                                            <c:if test="${(user.avatar == null) or (user.avatar.isEmpty())}">
                                                                <img class="img-fluid img-responsive rounded-circle mr-2" src="./img/avatar.jpg" alt="" width="50" class="rounded-circle">
                                                            </c:if>
                                                            <c:if test="${user.avatar != null}">
                                                                <img class="img-fluid img-responsive rounded-circle mr-2" src="./img/uploads/${user.avatar}" alt="" width="50" class="rounded-circle">
                                                            </c:if>
                                                            <input type="hidden" name="songID" value="${cm.songid}">
                                                            <input type="hidden" name="commentID" value="${cm.commentid}">

                                                            <div class="comment-text w-100">
                                                                <input type="text" name="content" class="form-control mr-3" placeholder="Reply comment">
                                                            </div>
                                                            <button class="btn btn-cyan btn-sm" type="submit">Reply Comment</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </c:if>
                            </c:forEach>
                        </div> 
                    </div>
                </div>
            </div>                  
        </section>

        <script>
            function myFunction() {
                document.getElementById("myForm").submit();
            }
            function myFunction1() {
                document.getElementById("myForm1").submit();
            }
        </script>




        <section id="newletter">
            <div class="jarallax bg-overlay bg-img" style="background-image: url('./img/core-img/jarallax.jpg');">
                <div class="container newletter__container">
                    <div class="newletter__content">
                        <h1>Sign Up To Newsletter</h1>
                        <p>Subscribe to receive info on our latest news and episodes</p>
                    </div>

                    <div class="newletter__subcribe">
                        <form action="sendnew" method="get">
                            <input type="text" placeholder="Your Email" name="email">
                            <input type="submit" value="SUBCRIBE">
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section id="footer">
            <div class="container footer__container" style="display: flex">
                <div class="footer__about"> 
                    <h2>About Us</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                    <p>&copy; Copyright &copy; 2022 <span>Group Four</span></p>
                </div>
                <ul class="footer__categories"> 
                    <h2>Categories</h2>
                    <li><a href="#" style="color: black">Entrepreneurship </a></li>
                    <li><a href="#" style="color: black">Media </a></li>
                    <li><a href="#" style="color: black">Tech </a></li>
                    <li>   <a href="#" style="color: black">Tutorials </a></li>
                </ul>
                <div class="footer_social"> 
                    <h2>Follow Us</h2>
                    <ul class="media">
                        <li><a class="fab fa-facebook" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-twitter" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-pinterest" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-instagram" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-youtube" href="#" style="color: black"> </a></li>
                    </ul>
                    <ul class="store"> 
                        <li> <a href=""><img src="./img/core-img/app-store.png" alt=""></a></li>
                        <li> <a href=""><img src="./img/core-img/google-play.png" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </section>


        <section id="toast"></section>
        <audio id="audio" src="">    </audio>
        <script src="./node_modules/jarallax/dist/jarallax.min.js"></script>
        <script src="./js/app.js"></script>
        <script src="./js/jquery-3.2.1.min.js"></script>
        <script src="./js/handle_ajax.js"></script>
        <script src="./js/handle_toast.js"></script>
        <!--<script src="./js/lyric.js"></script>-->
    </body>
</html>
