<%-- 
    Document   : profile
    Created on : May 19, 2022, 11:22:51 PM
    Author     : Thanh Thao
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="javax.mail.internet.MailDateFormat"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="model.Users"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

        <script src="js/profile.js"></script>
        <link href="css/profile.css" rel="stylesheet" type="text/css"/>
        <!--        <script>
                    $(document).ready(function() {
                    $('#new-password, #verify-password').on('keyup', function () {
                    if ($('#new-password').val() === $('#verify-password').val()) {
                    $('#message').html('Mật khẩu tương xứng').css('color', 'green');
                    } else
                            $('#message').html('Mật khẩu không tương xứng').css('color', 'red');
                    });
                    }
                    );
                </script>-->
    </head>
    <body style="background-color: gray">
        <div class="container-xl px-4 mt-4 "  >
            <!-- Account page navigation-->

            <hr class="mt-0 mb-4">
            <div class="row" >
                <div class="col-xl-4">
                    <!-- Profile picture card-->
                    <div class="card mb-4 mb-xl-0">
                        <div class="card-header">Profile Picture</div>
                        <div class="card-body text-center">
                            <!-- Profile picture image-->
                            <c:if test="${(user.avatar == null) or (user.avatar.isEmpty())}">
                                <img class="img-account-profile rounded-circle mb-2" src="./img/avatar.jpg" alt="">
                            </c:if>
                            <c:if test="${user.avatar != null}">
                                <img class="img-account-profile rounded-circle mb-2" src="./img/uploads/${user.avatar}" alt="">
                            </c:if>
                            <!-- Profile picture help block-->
                            <div class="small font-italic text-muted mb-4">JPG, JPEG, PNG or GIF no larger than 10MB</div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8" >
                    <!-- Account details card-->
                    <div class="card mb-4" style="padding: 20px; background-color: lavenderblush">
                        <div class="card-header">Profile Detail</div>

                        <%
                            if (session.getAttribute("mess") != null) {
                        %>
                        <h2 id="h2" style="color:orange">${sessionScope.mess}</h2>
                        <%
                                session.removeAttribute("mess");
                            }
                        %>  

                        <form id="profile_form" action="UpdateProfile" method="post" enctype="multipart/form-data">
                            <!-- Form Row-->
                            <div class="row gx-3 mb-3">
                                <!-- Form Group (first name)-->
                                <div class="col-md-6">
                                    <label class="small mb-1" for="inputFirstName">First name</label>
                                    <input class="form-control" id="inputFirstName" name="firstName" type="text" placeholder="Enter your first name" value="${user.first_name}">
                                </div>
                                <!-- Form Group (last name)-->
                                <div class="col-md-6">
                                    <label class="small mb-1" for="inputLastName">Last name</label>
                                    <input class="form-control" id="inputLastName" name="lastName" type="text" placeholder="Enter your last name" value="${user.last_name}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <label class="small mb-2" for="gender">Gender: </label>
                                <input  type="radio" name="gender" value="Male" ${user.gender =="Male"?"checked":""}/>Male
                                <input type="radio" name="gender" value="Female" ${user.gender == "Female"?"checked":""}/>Female
                            </div>
                            <!-- Form Group (username)-->
                            <div class="mb-3">
                                <label class="small mb-1" for="inputAvatar">Avatar</label>
                                <input type="file" class="form-control" name="photo">
                            </div>
                            <div><p style="color: red">${requestScope.error}</p></div>
                            <div class="mb-3">
                                <label class="small mb-1" for="inputUsername">Username </label>
                                <input class="form-control" id="inputUsername" type="text" placeholder="Enter your username" value="${user.username}" readonly="">
                            </div>
                            <div class="mb-3">
                                <label class="small mb-1" for="inputPassword">Password </label>

                                <input class="form-control" id="inputPassword"  type="password" name="old-password" placeholder="Old Password">
                                <input class="form-control" id="inputPassword"  type="password" id="new-password" name="new-password" placeholder="New Password" onChange="onChange()">
                                <input class="form-control" id="inputPassword"  type="password" id="verify-password" name="verify-password" placeholder="Verify Password" onChange="onChange()">
                                <span id='message'></span>

                            </div>

                            <!-- Form Group (email address)-->
                            <div class="mb-3">
                                <label class="small mb-1" for="inputEmailAddress">Email address</label>
                                <input class="form-control" id="inputEmailAddress" required="" name="email" type="email" placeholder="Enter your email address" value="${user.email}">
                            </div>
                            <div class="mb-3">
                                <label class="small mb-1" for="inputEmailAddress">Address</label>
                                <input class="form-control" id="inputEmailAddress" required="" name="address" placeholder="Enter your address" value="${user.address}">
                            </div>
                            <!-- Form Row-->
                            <div class="row gx-3 mb-3">
                                <!-- Form Group (phone number)-->
                                <!--                                    <div class="col-md-6">
                                                                        <label class="small mb-1" for="inputPhone">Phone number</label>
                                                                        <input class="form-control" id="inputPhone" type="tel" placeholder="Enter your phone number" value="555-123-4567">
                                                                    </div>-->
                                <!-- Form Group (birthday)-->
                                <div class="col-md-6">
                                    <label class="small mb-1" for="inputBirthday">Birthday</label> 
                                    <%
                                        Users user = (Users) session.getAttribute("user");
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                        Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(user.getDob());
                                        SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
                                        String dates = df2.format(date1);
                                    %>
                                    <input class="form-control" id="inputBirthday"  type="date" name="birthday" value="<%=dates%>">

                                </div>
                            </div>
                            <!-- Save changes button-->
                            <button class="btn btn-primary" type="submit">Save changes</button>

                            <div>
                                <%
                                    boolean error = false;
                                    if (request.getAttribute("erroOldPass") != null) {
                                        error = Boolean.parseBoolean(request.getAttribute("erroOldPass").toString());
                                    }
                                    if (error) {%>
                                <p style="color: red">mật khẩu cũ không đúng!</p>
                                <%}
                                %>
                            </div>
                            <div>
                                <%
                                    boolean errorEmail = false;
                                    if (request.getAttribute("erroEmail") != null) {
                                        errorEmail = Boolean.parseBoolean(request.getAttribute("erroEmail").toString());
                                    }
                                    if (errorEmail) {%>
                                <p style="color: red">Email này đã tồn tại!</p>
                                <%}
                                %>
                            </div>
                        </form>
                        <br>
                        <a href="/SE1607_Group4_OnlineMusic/home"><button class="btn btn-primary" >Back To Home</button></a>
                    </div>
                </div>
            </div>
        </div>
    <!--</div>-->
</body>
</html>