/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.LikeDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Like;
import model.Users;

/**
 *
 * @author Auriat
 */
public class ManageLikeServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ViewLikeServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ViewLikeServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        Users u = (Users) session.getAttribute("user");
        LikeDAO dao = new LikeDAO();
        String operation = request.getParameter("operation");
        if (operation == null) {
            if (u == null) {
                request.getRequestDispatcher("login.jsp").forward(request, response);
            } else if (u.getRole_ID() == 1 || u.getRole_ID() == 2) {
                List<Like> list = dao.getSongByLike();
                request.setAttribute("listS", list);

                request.getRequestDispatcher("home.jsp").forward(request, response);
            }
        } else if (operation.equalsIgnoreCase("like")) {
            int flag = Integer.parseInt(request.getParameter("flag"));
            int songID = Integer.parseInt(request.getParameter("songID"));
            if (flag == 0) {
                dao.insertLike(u.getUser_ID(), songID, true);
            } else {
                dao.cancelLike(u.getUser_ID(), songID);
            }
            response.sendRedirect("home");
        } else if (operation.equalsIgnoreCase("likecomment")) {

            int songID = Integer.parseInt(request.getParameter("songID"));
            int commentID = Integer.parseInt(request.getParameter("commentID"));
            String flag = request.getParameter("flag");
            
            if (request.getParameter("flag").equalsIgnoreCase("")) {
                
                dao.updateLikeComment(u.getUser_ID(), commentID, songID, true);
            }
            else if (flag.equalsIgnoreCase("0")) {
                dao.cancelLikeComment(u.getUser_ID(), songID, commentID);
            }

            response.sendRedirect("songDetail?id=" + songID);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Users u = (Users) session.getAttribute("user");
        boolean check = request.getParameter("like") != null;
        int songID = Integer.parseInt(request.getParameter("songID"));
        LikeDAO dao = new LikeDAO();
        if (check) {
            dao.insertLike(u.getUser_ID(), songID, check);
        } else {
            dao.cancelLike(u.getUser_ID(), songID);
        }
        response.sendRedirect("songDetail?id=" + songID);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
