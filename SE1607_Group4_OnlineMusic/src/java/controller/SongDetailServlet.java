/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.HistoryViewDAO;
import dal.CommentDAO;
import dal.LikeDAO;
import dal.LyricsDAO;
import dal.ReplyCommentDAO;
import dal.SongsDAO;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Comment;
import model.Like;
import model.Lyrics;
import model.ReplyComment;
import model.Songs;
import model.Users;

/**
 *
 * @author Auriat
 */
public class SongDetailServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int songID = Integer.parseInt(request.getParameter("id"));
        SongsDAO ad = new SongsDAO();
        LyricsDAO l = new LyricsDAO();
        LikeDAO dao = new LikeDAO();
        
        HttpSession session = request.getSession();
        Users u = (Users) session.getAttribute("user"); 
        
        Songs song = ad.getSongsByID(songID);
        ad.addViewsBySongID(songID);
        
        ArrayList<Lyrics> lyrics = l.getLyricsWithSongID(songID);
        request.setAttribute("slider", song);
        request.setAttribute("lyrics", lyrics);
        HistoryViewDAO hd = new HistoryViewDAO();
        
        if(u != null){
            int user_ID = u.getUser_ID();
            hd.addHistoryView(user_ID, songID);
            int userTotalViews = hd.getUserTotalViewBySongID(songID, user_ID);
            request.setAttribute("userTotalViews", userTotalViews);
            Timestamp userLastVisited = hd.getUserLastVisitedBySongID(songID, user_ID);
            request.setAttribute("userLastVisited", userLastVisited);
        }
        
        ArrayList<Songs> albums_Vpop = ad.getSongsOfCategory(1);
        request.setAttribute("albums_Vpop", albums_Vpop);
        ArrayList<Songs> albums_USUK = ad.getSongsOfCategory(2);
        request.setAttribute("albums_USUK", albums_USUK);
        ArrayList<Songs> albums_Lofi = ad.getSongsOfCategory(3);
        request.setAttribute("albums_Lofi", albums_Lofi);
        
        CommentDAO cm = new CommentDAO();
        ReplyCommentDAO replyDAO = new ReplyCommentDAO();
        
        ArrayList<Users> user = cm.getAllUserByCommentID();
        request.setAttribute("user1", user);
        
        ArrayList<Comment> comment = cm.getAllCommentBySongID(songID);
        request.setAttribute("cmm", comment);
        
        ArrayList<ReplyComment> reply = replyDAO.getReplyComment();
        request.setAttribute("reply", reply);

       
        
        if (session.getAttribute("user") != null) {
            boolean check = dao.duplicateLike(u.getUser_ID(), songID);
            request.setAttribute("checkerviet", check);
            
            ArrayList<Like> listLike = dao.getCheckLikeComment(u.getUser_ID());
            request.setAttribute("likeCheck", listLike);

            ArrayList<Like> checkcmt = dao.duplicateLikeComment(u.getUser_ID(), songID);
            request.setAttribute("checker", checkcmt);
        }
        request.getRequestDispatcher("songDetail.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        LikeDAO dao = new LikeDAO();
        HttpSession session = request.getSession();
        Users u = (Users) session.getAttribute("user");
        int songID = Integer.parseInt(request.getParameter("songID"));
        int commentID = Integer.parseInt(request.getParameter("commentID"));
        boolean check = request.getParameter("likecomment") != null;
        if (check) {
            dao.updateLikeComment(u.getUser_ID(), commentID, songID, check);
        } else {
            dao.cancelLikeComment(u.getUser_ID(), songID, commentID);
        }

        response.sendRedirect("songDetail?id=" + songID);
        
        
        
    }
}
