/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.LikeDAO;
import dal.SliderDAO;
import dal.SongsDAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Categories;
import model.Like;
import model.Slider;
import model.Songs;
import model.Users;

/**
 *
 * @author Admin
 */
public class HomeServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LikeDAO dao = new LikeDAO();
        SliderDAO sd = new SliderDAO();
        ArrayList<Slider> sliders = sd.getAll();
        request.setAttribute("sliders", sliders);
        SongsDAO ad = new SongsDAO();
        HttpSession session = request.getSession();
        Users u = (Users) session.getAttribute("user");
        ArrayList<Songs> albums_Vpop = ad.getSongsOfCategory(1);
        request.setAttribute("albums_Vpop", albums_Vpop);
        ArrayList<Songs> albums_USUK = ad.getSongsOfCategory(2);
        request.setAttribute("albums_USUK", albums_USUK);
        ArrayList<Songs> albums_Lofi = ad.getSongsOfCategory(3);
        request.setAttribute("albums_Lofi", albums_Lofi);

        ArrayList<Songs> newsong = ad.getNewSong();
        request.setAttribute("newsong", newsong);
        
        ArrayList<Categories> catename = ad.getCategoryName();
        request.setAttribute("catename", catename);

        ArrayList<Songs> hotsong = ad.getSongHot();
        request.setAttribute("hotsong", hotsong);

        List<Like> list = dao.getSongByLike();
        request.setAttribute("listS", list);

        if(u != null){
        ArrayList<Like> checkLikeSong = dao.getCheckLikeSong(u.getUser_ID());
        request.setAttribute("checkLikeSong", checkLikeSong);
        }
        
        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
