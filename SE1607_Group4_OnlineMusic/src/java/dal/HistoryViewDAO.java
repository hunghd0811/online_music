/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.HistoryView;

/**
 *
 * @author huanv
 */
public class HistoryViewDAO extends BaseDAO<HistoryView>{
    public List<HistoryView> getAllHistoryViewByUser(int user_ID){
        List<HistoryView> list = new ArrayList<>();
        String sql = "select h.history_ID, h.t_create, u.user_ID, s.song_ID, u.username, s.name, s.author, s.duration, s.image from HistoryView h inner join Users u on h.user_ID = u.user_ID inner join Songs s on h.song_ID = s.song_ID where h.user_ID = ? and h.user_ID is not null order by t_create desc";
        try{
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, user_ID);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                list.add(new HistoryView(rs.getInt(1), rs.getTimestamp(2), rs.getInt(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return list;
    }
    
    public void addHistoryView(int user_ID, int song_ID){
        String sql = "insert into HistoryView values (GETDATE(), ?, ?)";
        try{
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, user_ID);
            st.setInt(2, song_ID);
            st.executeUpdate();
        }catch(SQLException e){
            System.out.println(e);
        }
    }

    public List<HistoryView> getAllHistoryView(){
        List<HistoryView> list = new ArrayList<>();
        String sql = "select h.history_ID, h.t_create, u.user_ID, s.song_ID, u.username, s.name, s.author, s.duration, s.image from HistoryView h inner join Users u on h.user_ID = u.user_ID inner join Songs s on h.song_ID = s.song_ID where h.user_ID is not null order by t_create desc";
        try{
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                list.add(new HistoryView(rs.getInt(1), rs.getTimestamp(2), rs.getInt(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return list;
    }
    
    public int getUserTotalViewBySongID(int song_ID, int user_ID){
        String sql = "select count(song_ID) as [Views] from HistoryView where song_ID = ? and user_ID = ? and user_ID is not null";
        try{
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, song_ID);
            st.setInt(2, user_ID);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return rs.getInt(1);
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return 0;
    }
    
    public Timestamp getUserLastVisitedBySongID(int song_ID, int user_ID){
        String sql = "select max(t_create) from HistoryView where t_create not in (select max(t_create) from HistoryView where song_ID = ? and user_ID = ?) and song_ID = ? and user_ID = ?";
        try{
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, song_ID);
            st.setInt(2, user_ID);
            st.setInt(3, song_ID);
            st.setInt(4, user_ID);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return rs.getTimestamp(1);
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return null;
    }
    @Override
    public ArrayList<HistoryView> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
