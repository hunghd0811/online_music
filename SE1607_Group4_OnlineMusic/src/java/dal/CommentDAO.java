/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Comment;
import model.Songs;
import model.Users;

/**
 *
 * @author anhqu
 */
public class CommentDAO extends BaseDAO<Comment> {

    @Override
    public ArrayList<Comment> getAll() {
        ArrayList<Comment> list = new ArrayList<>();
        try {
            String sql = "select comment_ID,song_ID,user_ID,comment,status,[like],t_create,t_lastUpdate,report from Comments where report = 1";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Comment cm = new Comment();
                cm.setCommentid(rs.getInt(1));
                cm.setSongid(rs.getInt(2));
                cm.setUserid(rs.getInt(3));
                cm.setComment(rs.getString(4));
                cm.setStatus(rs.getBoolean(5));
                cm.setLike(rs.getInt(6));
                cm.setTcreate(rs.getTimestamp(7));
                cm.setTlastupdate(rs.getTimestamp(8));
                cm.setReport(rs.getBoolean(9));
                list.add(cm);
            }

        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }

    public void updateStatusComment(int commentid, boolean status) {
        try {
            String sql = "UPDATE Comments\n"
                    + "SET status = ?,report = 0 WHERE comment_ID = ?";
            PreparedStatement st = connection.prepareCall(sql);
            st.setBoolean(1, status);
            st.setInt(2, commentid);
            st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Comment getCommentByID(int commntid) {
        try {
            String sql = "select comment_ID,song_ID,user_ID,"
                    + "comment,status,[like],t_create,t_lastUpdate from Comments "
                    + "where comment_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, commntid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Comment cm = new Comment();
                cm.setCommentid(rs.getInt(1));
                cm.setSongid(rs.getInt(2));
                cm.setUserid(rs.getInt(3));
                cm.setComment(rs.getString(4));
                cm.setStatus(rs.getBoolean(5));
                cm.setLike(rs.getInt(6));
                cm.setTcreate(rs.getTimestamp(7));
                cm.setTlastupdate(rs.getTimestamp(8));
                return cm;
            }

        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Comment> getAllCommentBySongID(int song_id) {
        ArrayList<Comment> list = new ArrayList<>();
        try {
            String sql = "select comment_ID, song_ID,user_ID, comment,status,[like],t_create,t_lastUpdate,report from Comments where song_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, song_id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Comment cm = new Comment();
                cm.setCommentid(rs.getInt(1));
                cm.setSongid(rs.getInt(2));
                cm.setUserid(rs.getInt(3));
                cm.setComment(rs.getString(4));
                cm.setStatus(rs.getBoolean(5));
                cm.setLike(rs.getInt(6));
                cm.setTcreate(rs.getTimestamp(7));
                cm.setTlastupdate(rs.getTimestamp(8));
                cm.setReport(rs.getBoolean(9));
                list.add(cm);
            }

        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }

    public ArrayList<Users> getAllUserByCommentID() {
        ArrayList<Users> list = new ArrayList<>();
        try {
            String sql = "select user_ID,last_name,first_name,avatar from Users ";
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Users u = new Users();
                u.setUser_ID(rs.getInt(1));
                u.setLast_name(rs.getString(2));
                u.setFirst_name(rs.getString(3));
                u.setAvatar(rs.getString(4));
                list.add(u);

            }

        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }

    public void insertComment(int songid, int userid, String comment) {
        String sql = "INSERT INTO Comments ([song_ID],"
                + "[user_ID],[comment] ,[status],"
                + "t_create,[like],report) VALUES (?,?,?,1,GETDATE(),0,0)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, songid);
            st.setInt(2, userid);
            st.setString(3, comment);
            st.executeUpdate();
            st.close();
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Songs> getAllSong() {
        ArrayList<Songs> songs = new ArrayList<>();
        try {
            String sql = "SELECT song_ID,name FROM Songs";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt(1));
                s.setName(rs.getString(2));
                songs.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return songs;
    }

    public ArrayList<Users> getAllUser() {
        ArrayList<Users> users = new ArrayList<>();
        try {
            String sql = "SELECT user_ID,first_name, last_name "
                    + "FROM Users";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            try {
                while (rs.next()) {
                    Users u = new Users();
                    u.setUser_ID(rs.getInt("user_ID"));
                    u.setFirst_name(rs.getString("first_name"));
                    u.setLast_name(rs.getString("last_name"));
                    users.add(u);
                }
            } catch (Exception e) {
            }
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return users;
    }

    public void updateReportComment(int commentid, boolean report) {
        try {
            String sql = "UPDATE Comments\n"
                    + "SET report = ? WHERE comment_ID = ?";
            PreparedStatement st = connection.prepareCall(sql);
            st.setBoolean(1, report);
            st.setInt(2, commentid);
            st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateStatusAllComment() {
        try {
            String sql = "UPDATE Comments\n"
                    + "SET status = 0 WHERE report = 1";
            PreparedStatement st = connection.prepareCall(sql);
            st.executeUpdate();
            String sql1 = "UPDATE Comments\n"
                    + "SET report = 0 WHERE status = 0";
            PreparedStatement st1 = connection.prepareCall(sql1);
            st1.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteComment(int commentID) {
        try {
            String sql = "DELETE FROM [dbo].[ReplyComment]\n"
                    + "      WHERE comment_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, commentID);
            st.executeUpdate();
            String sql1 = "DELETE FROM [dbo].[Liked]\n"
                    + "      WHERE comment_ID = ?";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.setInt(1, commentID);
            st1.executeUpdate();
            String sql2 = "DELETE FROM [dbo].[Comments]\n"
                    + "      WHERE comment_ID = ?";
            PreparedStatement st2 = connection.prepareStatement(sql2);
            st2.setInt(1, commentID);
            st2.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editComment(int userID, String comment, int commentID, int songID) {
        try {
            String sql = "UPDATE [dbo].[Comments]\n"
                    + "   SET [song_ID] = ?\n"
                    + "      ,[user_ID] = ?\n"
                    + "      ,[comment] = ?\n"
                    + "      ,[t_lastUpdate] = GETDATE()\n"
                    + " WHERE Comment_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, songID);
            st.setInt(2, userID);
            st.setString(3, comment);
            st.setInt(4, commentID);
            st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
