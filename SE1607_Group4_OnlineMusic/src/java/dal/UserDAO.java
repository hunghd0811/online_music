/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Users;

/**
 *
 * @author dclon
 */
public class UserDAO extends BaseDAO<Users> {

    public Users check(String user, String pass) {
        Users d = null;
        String sql = "select user_ID,username,password,first_name,"
                + "last_name,avatar,t_create,t_lastOnline,email,status,address,"
                + "gender,DOB,role_ID,randomVerify,report from Users where username=? and password=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, pass);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Users u = new Users();
                    u.setUser_ID(rs.getInt(1));
                    u.setUsername(rs.getString(2));
                    u.setPassword(rs.getString(3));
                    u.setFirst_name(rs.getString(4));
                    u.setLast_name(rs.getString(5));
                    u.setAvatar(rs.getString(6));
                    u.setT_create(rs.getTimestamp(7));
                    u.setT_lastOnline(rs.getTimestamp(8));
                    u.setEmail(rs.getString(9));
                    u.setStatus(rs.getBoolean(10));
                    u.setAddress(rs.getString(11));
                    u.setGender(rs.getString(12));
                    u.setDob(rs.getString(13));
                    u.setRole_ID(rs.getInt(14));
                    u.setRandom(rs.getString(15));
                    u.setReport(rs.getBoolean(16));
                return u;
            }
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public ArrayList<Users> getAll() {
        ArrayList<Users> users = new ArrayList<>();
        try {
            String sql = "SELECT user_ID, username, password, first_name, last_name, "
                    + "avatar, role_ID, t_create, t_lastOnline, email\n"
                    + "FROM Users";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            try {
                while (rs.next()) {
                    Users u = new Users();
                    u.setUser_ID(rs.getInt("user_ID"));
                    u.setUsername(rs.getString("username"));
                    u.setPassword(rs.getString("password"));
                    u.setFirst_name(rs.getString("first_name"));
                    u.setLast_name(rs.getString("last_name"));
                    u.setAvatar(rs.getString("avatar"));
                    u.setRole_ID(rs.getInt("role_ID"));
                    u.setT_create(rs.getTimestamp("t_create"));
                    u.setT_lastOnline(rs.getTimestamp("t_lastOnline"));
                    u.setEmail(rs.getString("email"));
                    users.add(u);
                }
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
        return users;
    }

    public void updateStatus(int id, boolean status) {
        try {
            String sql = "UPDATE Users\n"
                    + "SET status = ? WHERE user_ID = ?";
            PreparedStatement st = connection.prepareCall(sql);
            st.setBoolean(1, status);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void updateTimeLastOnline(int id) {
        try {
            String sql = "UPDATE Users\n"
                    + "SET t_lastOnline = GETDATE() WHERE user_ID = ?";
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public Users updateUser(String username, String getFirst_name, String getLast_name, String oldPassword, String newPassword, String getEmail, String getAddress, String isGender, String getDob, String getAvatar, int getUser_ID) {
        try {
            if (!newPassword.equals("")) {
                String sql = "Update Users\n"
                        + "SET first_name = ?, last_name = ?, password = ?, email=?,address=?,gender=?,DOB=?,avatar=?\n"
                        + "WHERE user_ID = ?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setString(1, getFirst_name);
                st.setString(2, getLast_name);
                st.setString(3, newPassword);
                st.setString(4, getEmail);
                st.setString(5, getAddress);
                st.setString(6, isGender);
                st.setString(7, getDob);
                st.setString(8, getAvatar);
                st.setInt(9, getUser_ID);
                st.executeUpdate();
                Users u = check(username, newPassword);
                return u;
            } else {
                String sql = "Update Users\n"
                        + "SET first_name = ?, last_name = ?, email=?,address=?,gender=?,DOB=?,avatar=?\n"
                        + "WHERE user_ID = ?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setString(1, getFirst_name);
                st.setString(2, getLast_name);
                st.setString(3, getEmail);
                st.setString(4, getAddress);
                st.setString(5, isGender);
                st.setString(6, getDob);
                st.setString(7, getAvatar);
                st.setInt(8, getUser_ID);
                st.executeUpdate();
                Users u = check(username, oldPassword);
                return u;
            }

        } catch (Exception e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }
    
    public Users updateUser2(String username, String getFirst_name, String getLast_name, String oldPassword, String newPassword, String getEmail, String getAddress, String isGender, String getDob, int getUser_ID) {
        try {
            if (!newPassword.equals("")) {
                String sql = "Update Users\n"
                        + "SET first_name = ?, last_name = ?, password = ?, email=?,address=?,gender=?,DOB=?\n"
                        + "WHERE user_ID = ?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setString(1, getFirst_name);
                st.setString(2, getLast_name);
                st.setString(3, newPassword);
                st.setString(4, getEmail);
                st.setString(5, getAddress);
                st.setString(6, isGender);
                st.setString(7, getDob);
                st.setInt(8, getUser_ID);
                st.executeUpdate();
                Users u = check(username, newPassword);
                return u;
            } else {
                String sql = "Update Users\n"
                        + "SET first_name = ?, last_name = ?, email=?,address=?,gender=?,DOB=?\n"
                        + "WHERE user_ID = ?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setString(1, getFirst_name);
                st.setString(2, getLast_name);
                st.setString(3, getEmail);
                st.setString(4, getAddress);
                st.setString(5, isGender);
                st.setString(6, getDob);
                st.setInt(7, getUser_ID);
                st.executeUpdate();
                Users u = check(username, oldPassword);
                return u;
            }

        } catch (Exception e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    public Users checkAccountExist(String username, String email) {
        String sql = "select * from Users where [username] = ? or [email] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, email);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Users(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getTimestamp(7), rs.getTimestamp(8), rs.getString(9), rs.getBoolean(10), rs.getString(11), rs.getString(12), rs.getString(13), rs.getInt(14));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public boolean signup(String username, String password, String email,String date,String gender,String RandomString) {
        String sql = "insert into Users([username], [password], [t_create], [email],DOB, [status],gender, [role_ID], [randomVerify], [report]) values (?, ?, GETDATE(), ?, ?,0,? ,3, ?, 0)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, password);
            st.setString(3, email);
            st.setString(4, date);
            st.setString(5, gender);
            st.setString(6, RandomString);
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }

    public Users getUserByRandomVerify(String random) {
        String sql = "select user_ID,username,password,email,status from [Users] where randomVerify  = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, random);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Users u = new Users(rs.getInt("user_ID"), rs.getString("username"), rs.getString("password"), rs.getString("email"), rs.getBoolean("status"));
                return u;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public List<Users> getAllUser() {
        List<Users> list = new ArrayList<>();
        String sql = "select user_ID,username,password,first_name,last_name,avatar,"
                + "t_create,t_lastOnline,email,status,address,gender,DOB,role_ID,randomVerify,report from Users";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Users(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getTimestamp(7),
                        rs.getTimestamp(8),
                        rs.getString(9),
                        rs.getBoolean(10),
                        rs.getString(11),
                        rs.getString(12),
                        rs.getString(13),
                        rs.getInt(14),
                        rs.getString(15),
                        rs.getBoolean(16)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    
    

    public Users getAllUserByID(String id) {
        String sql = "select user_ID,username,password,first_name,last_name,avatar,"
                + "t_create,t_lastOnline,email,status,address,gender,DOB,role_ID from Users where user_ID = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return (new Users(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getTimestamp(7),
                        rs.getTimestamp(8),
                        rs.getString(9),
                        rs.getBoolean(10),
                        rs.getString(11),
                        rs.getString(12),
                        rs.getString(13),
                        rs.getInt(14)));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public Users getNewestUser() {
        String sql = "select user_ID,randomVerify from Users order by user_ID desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Users u = new Users();
                u.setUser_ID(rs.getInt(1));
                u.setRandom(rs.getString(2));
                return u;
            }
        } catch (SQLException e) {
        }
        return null;
    }
    public boolean updatePassword(int id, String pass) {
        try {
            String sql = "UPDATE Users\n"
                    + "SET password = ? WHERE user_ID = ?";
            PreparedStatement st = connection.prepareCall(sql);
            st.setString(1, pass);
            st.setInt(2, id);
            st.executeUpdate();
            return true;
        } catch (Exception e) {
        }
        return false;
    }
    public boolean ReportBlock(String email, boolean report) {
        try {
            String sql = "UPDATE Users\n"
                    + "SET report = ? WHERE email = ?";
            PreparedStatement st = connection.prepareCall(sql);
            st.setBoolean(1, report);
            st.setString(2, email);
            st.executeUpdate();
            return true;
        } catch (Exception e) {
        }
        return false;
    }
    
      public boolean getEmail(String email) {
        try {
            String sql = "select email from Users where email = ?";
            PreparedStatement st = connection.prepareCall(sql);
            st.setString(1, email);
          
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }
 
}
