/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Users;

/**
 *
 * @author anhqu
 */
public class EmailDAO extends BaseDAO<Users> {

    public ArrayList<Users> getAll() {
        ArrayList<Users> users = new ArrayList<>();
        try {
            String sql = "SELECT user_ID, username, password, first_name, last_name, "
                    + "avatar, role_ID, t_create, t_lastOnline, email\n"
                    + "FROM Users";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            try {
                while (rs.next()) {
                    Users u = new Users();
                    u.setUser_ID(rs.getInt("id"));
                    u.setUsername(rs.getString("username"));
                    u.setPassword(rs.getString("password"));
                    u.setFirst_name(rs.getString("first_name"));
                    u.setLast_name(rs.getString("last_name"));
                    u.setAvatar(rs.getString("avatar"));
                    u.setRole_ID(rs.getInt("role_ID"));
                    u.setT_create(rs.getTimestamp("t_create"));
                    u.setT_lastOnline(rs.getTimestamp("t_lastOnline"));
                    u.setEmail(rs.getString("email"));
                    users.add(u);
                }
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
        return users;
    }

   

    public Users getUserByEmail(String email) {
        String sql = "select user_ID,password,email,randomVerify from [Users] where email= ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Users u = new Users();
                u.setUser_ID(rs.getInt(1));
                u.setPassword(rs.getString(2));              
                u.setEmail(rs.getString(3));
                u.setRandom(rs.getString(4));
                
                return u;
            }
        } catch (SQLException e) {
        }

        return null;
    }
    
     public void updatePassword(String password,String email){
         String sql = "update Users set password = ? where email = ?";
         try {
             PreparedStatement st = connection.prepareStatement(sql);
             st.setString(1, password);
            st.setString(2, email);
            st.executeUpdate();
         } catch (Exception e) {
         }
      
     }
     public void updateRandomString(String email, String random){
         String sql = "update Users set randomVerify = ? where email = ?";
         try {
             PreparedStatement st = connection.prepareStatement(sql);
             st.setString(1, random);
            st.setString(2, email);
          st.executeUpdate();    
         } catch (Exception e) {
         }
       
     }

    
}
