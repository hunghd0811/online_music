/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author Auriat
 */
public class Like extends Songs{
    private int id;
    private Timestamp t_lastUpdate;
    private int user_ID;
    private int song_ID;
    private boolean like;
    private int totalLike;
    private int comment_ID;
    private boolean flag;

    public Like() {
    }

    public int getComment_ID() {
        return comment_ID;
    }

    public void setComment_ID(int comment_ID) {
        this.comment_ID = comment_ID;
    }

    public Like(int id, Timestamp t_lastUpdate, int user_ID, int song_ID, boolean like) {
        this.id = id;
        this.t_lastUpdate = t_lastUpdate;
        this.user_ID = user_ID;
        this.song_ID = song_ID;
        this.like = like;
    }
    
    

    public Like(int id, Timestamp t_lastUpdate, int user_ID, int song_ID, boolean like, int totalLike) {
        this.id = id;
        this.t_lastUpdate = t_lastUpdate;
        this.user_ID = user_ID;
        this.song_ID = song_ID;
        this.like = like;
        this.totalLike = totalLike;
    }

    public int getTotalLike() {
        return totalLike;
    }

    public void setTotalLike(int totalLike) {
        this.totalLike = totalLike;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getT_lastUpdate() {
        return t_lastUpdate;
    }

    public void setT_lastUpdate(Timestamp t_lastUpdate) {
        this.t_lastUpdate = t_lastUpdate;
    }

    public int getUser_ID() {
        return user_ID;
    }

    public void setUser_ID(int user_ID) {
        this.user_ID = user_ID;
    }

    public int getSong_ID() {
        return song_ID;
    }

    public void setSong_ID(int song_ID) {
        this.song_ID = song_ID;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    
}
