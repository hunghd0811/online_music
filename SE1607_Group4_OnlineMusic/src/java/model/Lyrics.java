/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Thanh Thao
 */
public class Lyrics {
    private int lyricID;
    private String content;
    private String time;
    private int songID;

    public Lyrics() {
    }

    public Lyrics(int lyricID, String content, String time, int songID) {
        this.lyricID = lyricID;
        this.content = content;
        this.time = time;
        this.songID = songID;
    }

    public int getLyricID() {
        return lyricID;
    }

    public void setLyricID(int lyricID) {
        this.lyricID = lyricID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getSongID() {
        return songID;
    }

    public void setSongID(int songID) {
        this.songID = songID;
    }
    
    
    
}
