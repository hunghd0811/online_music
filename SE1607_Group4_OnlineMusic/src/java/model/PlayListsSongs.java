/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author Black
 */
@Data
@Builder
@Getter
@Setter
@ToString
public class PlayListsSongs {
    private Songs songs;
    private int playlistID;
    private int song_ID;
    
}
