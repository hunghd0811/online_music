/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author anhqu
 */
public class Comment {
    private int commentid;
    private int songid;
    private int userid;
    private String comment;
    private boolean status;
    private int like;
    private Timestamp tcreate;
    private Timestamp tlastupdate;
    private boolean report;

    public Comment() {
    }

    public Comment(int commentid, int songid, int userid, String comment, boolean status, int like, Timestamp tcreate, Timestamp tlastupdate, boolean report) {
        this.commentid = commentid;
        this.songid = songid;
        this.userid = userid;
        this.comment = comment;
        this.status = status;
        this.like = like;
        this.tcreate = tcreate;
        this.tlastupdate = tlastupdate;
        this.report = report;
    }

   

    public int getCommentid() {
        return commentid;
    }

    public void setCommentid(int commentid) {
        this.commentid = commentid;
    }

    public int getSongid() {
        return songid;
    }

    public void setSongid(int songid) {
        this.songid = songid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public Timestamp getTcreate() {
        return tcreate;
    }

    public void setTcreate(Timestamp tcreate) {
        this.tcreate = tcreate;
    }

    public Timestamp getTlastupdate() {
        return tlastupdate;
    }

    public void setTlastupdate(Timestamp tlastupdate) {
        this.tlastupdate = tlastupdate;
    }

    public boolean isReport() {
        return report;
    }

    public void setReport(boolean report) {
        this.report = report;
    }
    

    @Override
    public String toString() {
        return "Comment{" + "commentid=" + commentid + ", songid=" + songid + ", userid=" + userid + ", comment=" + comment + ", status=" + status + ", like=" + like + ", tcreate=" + tcreate + ", tlastupdate=" + tlastupdate + '}';
    }

    
   
    
}
